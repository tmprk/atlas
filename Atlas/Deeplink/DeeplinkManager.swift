//
//  DeeplinkManager.swift
//  Atlas
//
//  Created by Tim Park on 11/3/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit
import Foundation

enum DeeplinkType {
    case messages
    case activity
}

let Deeplinker = DeepLinkManager()
class DeepLinkManager {
    fileprivate init() {}
    
    private var deeplinkType: DeeplinkType?
    
    @discardableResult
    func handleShortcut(item: UIApplicationShortcutItem) -> Bool {
        deeplinkType = ShortcutParser.shared.handleShortcut(item)
        return deeplinkType != nil
    }
    
    // check existing deepling and perform action
    func checkDeepLink() {
        (UIApplication.shared.windows.first?.rootViewController as! RootViewController).deeplink = deeplinkType
        
        // reset deeplink after handling
        self.deeplinkType = nil
    }
}
