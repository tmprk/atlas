//
//  AppDelegate+Injection.swift
//  Atlas
//
//  Created by Tim Park on 1/17/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import UIKit

extension AppDelegate {
    func loadInjectionIII() {
        #if DEBUG
        Bundle(path: "/Applications/InjectionIII.app/Contents/Resources/iOSInjection.bundle")?.load()
        #endif
    }
}

extension UIViewController {
    
    @objc func injected() {
        viewDidLoad()
        viewWillAppear(true)
        viewDidAppear(true)
    }
    
}
