//
//  PatientCell.swift
//  Atlas
//
//  Created by Tim Park on 10/2/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

class PatientCell: BaseCell {
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = NSLayoutConstraint.Axis.horizontal
        stackView.distribution = UIStackView.Distribution.fill
        stackView.alignment = UIStackView.Alignment.fill
        stackView.spacing = 0
        return stackView
    }()
    
    let idLabel: UILabel = {
        let idLabel = UILabel()
        idLabel.translatesAutoresizingMaskIntoConstraints = false
        idLabel.lineBreakMode = .byWordWrapping
        idLabel.numberOfLines = 1
        idLabel.highlightedTextColor = .label
        idLabel.textColor = .label
        idLabel.font = .systemFont(ofSize: 22, weight: .semibold)
        return idLabel
    }()
    
    let infoLabel: UILabel = {
        let infoLabel = UILabel()
        infoLabel.translatesAutoresizingMaskIntoConstraints = false
        infoLabel.lineBreakMode = .byWordWrapping
        infoLabel.numberOfLines = 1
        infoLabel.highlightedTextColor = .label
        infoLabel.textAlignment = .right
        infoLabel.textColor = .systemBlue
        return infoLabel
    }()
    
    static var identifier: String {
        return String(describing: self)
    }
    
    func configure(patient: Patient) {
        self.idLabel.text = patient.uniqueID
        self.infoLabel.text = patient.type.rawValue
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupCell()
    }
    
}

extension PatientCell {
    
    func setupCell() {
        self.layer.cornerRadius = 4.5
        self.layer.masksToBounds = true
        self.backgroundColor = .secondarySystemBackground
        self.contentView.addSubview(stackView)
        stackView.addArrangedSubview(idLabel)
        stackView.addArrangedSubview(infoLabel)
        let labelInsets = UIEdgeInsets(top: 18, left: 16, bottom: -18, right: -16)
        let stackViewConstraints = [
            stackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: labelInsets.left),
            stackView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: labelInsets.top),
            stackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: labelInsets.right),
            stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: labelInsets.bottom)
        ]
        NSLayoutConstraint.activate(stackViewConstraints)
    }
    
}
