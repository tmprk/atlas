//
//  EventCell.swift
//  Atlas
//
//  Created by Tim Park on 11/11/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {
    
    var event: Events! {
        didSet {
            guard let eventDate = event.rescheduled else { return }
            self.eventTitle.text = event.name
            self.dateLabel.text = event.rescheduled!.localDateString()
            self.isUserInteractionEnabled = event.skipped ? false : event.completed ? true : false
            self.backgroundColor = event.skipped ? UIColor.systemOrange : event.completed ? UIColor.systemGreen : UIColor.systemRed.withAlphaComponent(0.8) 
            self.contentView.alpha = event.skipped ? 0.3 : (eventDate < Date()) && (event.completed) ? 1.0 : 0.3
            self.eventTitle.textColor = event.completed ? .white : self.backgroundColor!.mixDarker(amount: 0.95)
            self.dateLabel.textColor = event.completed ? .white : self.backgroundColor!.mixDarker(amount: 0.95)
        }
    }
    
    let eventTitle: UILabel = {
        let eventTitle = UILabel()
        eventTitle.translatesAutoresizingMaskIntoConstraints = false
        eventTitle.font = .systemFont(ofSize: 22, weight: .semibold)
        eventTitle.lineBreakMode = .byWordWrapping
        eventTitle.numberOfLines = 0
        eventTitle.textColor = .white
        return eventTitle
    }()
    
    let dateLabel: UILabel = {
        let dateLabel = UILabel()
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.font = .systemFont(ofSize: 18, weight: .regular)
        dateLabel.textAlignment = .left
        dateLabel.textColor = .label
//        dateLabel.backgroundColor = .systemPink
        return dateLabel
    }()
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupCell()
    }
    
}

extension EventCell {
    
    func setupCell() {
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.systemGreen.mixDarker(amount: 0.3)
        self.selectedBackgroundView = backgroundView
        
        self.contentView.addSubview(eventTitle)
        self.contentView.addSubview(dateLabel)
        let labelInsets = UIEdgeInsets(top: 16, left: 16, bottom: -16, right: -16)
        let constraints = [
            eventTitle.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: labelInsets.left),
            eventTitle.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: labelInsets.top),
            eventTitle.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: labelInsets.right),
            
            dateLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: labelInsets.left),
            dateLabel.topAnchor.constraint(equalTo: self.eventTitle.bottomAnchor, constant: labelInsets.top / 2),
            dateLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: labelInsets.right),
            dateLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: labelInsets.bottom)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
}
