//
//  AttributionCell.swift
//  Atlas
//
//  Created by Tim Park on 1/10/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import UIKit

class AttributionCell: UITableViewCell {
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView?.frame = CGRect( x: 16, y: 15, width: imageView!.intrinsicContentSize.width, height:  imageView!.intrinsicContentSize.height)
    }
    
}
