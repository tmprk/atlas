//
//  CenteredCell.swift
//  Atlas
//
//  Created by Tim Park on 1/9/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import UIKit

class CenteredCell: UITableViewCell {
    
    let actionLabel: UILabel = {
        let nl = UILabel()
        nl.translatesAutoresizingMaskIntoConstraints = false
        nl.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        nl.lineBreakMode = .byWordWrapping
        nl.numberOfLines = 0
        nl.textColor = .systemRed
        nl.textAlignment = .center
        return nl
    }()
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLabels()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLabels()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension CenteredCell {
    
    func setupLabels() {
        self.contentView.addSubview(actionLabel)
        let labelInsets = UIEdgeInsets(top: 4, left: 8, bottom: -4, right: -8)
        let actionLabelConstraints = [
            actionLabel.leadingAnchor.constraint(equalTo: self.contentView.layoutMarginsGuide.leadingAnchor, constant: labelInsets.left),
            actionLabel.topAnchor.constraint(equalTo: self.contentView.layoutMarginsGuide.topAnchor, constant: labelInsets.top),
            actionLabel.trailingAnchor.constraint(equalTo: self.contentView.layoutMarginsGuide.trailingAnchor, constant: labelInsets.right),
            actionLabel.bottomAnchor.constraint(equalTo: self.contentView.layoutMarginsGuide.bottomAnchor, constant: labelInsets.bottom)
        ]
        NSLayoutConstraint.activate(actionLabelConstraints)
    }
    
}
