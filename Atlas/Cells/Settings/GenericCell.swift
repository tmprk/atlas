//
//  GenericCell.swift
//  Atlas
//
//  Created by Tim Park on 1/9/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import UIKit

class GenericCell: UITableViewCell {
    
    lazy var stackView: UIStackView = {
        let sv = UIStackView(frame: .zero)
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.axis = NSLayoutConstraint.Axis.horizontal
        sv.distribution = UIStackView.Distribution.fillProportionally
        sv.alignment = UIStackView.Alignment.fill
        sv.spacing = 0.0
        return sv
    }()
    
    lazy var keyLabel: UILabel = {
        let nl = UILabel()
        nl.translatesAutoresizingMaskIntoConstraints = false
//        nl.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        nl.textColor = .label
        nl.lineBreakMode = .byWordWrapping
        nl.numberOfLines = 1
        nl.textColor = .darkGray
//        nl.backgroundColor = .systemPurple
        return nl
    }()
    
    lazy var valueLabel: UILabel = {
        let tl = UILabel()
        tl.translatesAutoresizingMaskIntoConstraints = false
//        tl.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        tl.textAlignment = .right
        tl.lineBreakMode = .byWordWrapping
        tl.numberOfLines = 1
        tl.textColor = .systemBlue
//        tl.backgroundColor = .systemPink
        return tl
    }()
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLabels()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLabels()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension GenericCell {
    
    // Update text
    func updateText(key: String, value: String) {
        keyLabel.text = key
        valueLabel.text = value
    }
    
    func setupLabels() {
        stackView.addArrangedSubview(keyLabel)
        stackView.addArrangedSubview(valueLabel)
        self.contentView.addSubview(stackView)
        let labelInsets = UIEdgeInsets(top: 16, left: 12, bottom: -16, right: -12)
        let keyLabelConstraints = [
            stackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: labelInsets.left),
            stackView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: labelInsets.top),
            stackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: labelInsets.right),
            stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: labelInsets.bottom)
        ]
        NSLayoutConstraint.activate(keyLabelConstraints)
    }
    
}
