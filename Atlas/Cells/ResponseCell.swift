//
//  ResponseCell.swift
//  Atlas
//
//  Created by Tim Park on 11/11/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

class ResponseCell: UITableViewCell {
    
    let fieldLabel: UILabel = {
        let fieldLabel = UILabel()
        fieldLabel.translatesAutoresizingMaskIntoConstraints = false
        fieldLabel.font = .systemFont(ofSize: 18, weight: .medium)
        fieldLabel.lineBreakMode = .byWordWrapping
        fieldLabel.numberOfLines = 0
        fieldLabel.highlightedTextColor = .label
        fieldLabel.textColor = .label
        return fieldLabel
    }()
    
    static var identifier: String {
        return String(describing: self)
    }
    
    func configure(fieldText: String) {
        self.fieldLabel.text = fieldText
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupCell()
    }
    
}

extension ResponseCell {
    
    func setupCell() {
        contentView.backgroundColor = .systemBackground
        self.contentView.addSubview(fieldLabel)
        let labelInsets = UIEdgeInsets(top: 18, left: 16, bottom: -18, right: -16)
        let stackViewConstraints = [
            fieldLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: labelInsets.left),
            fieldLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: labelInsets.top),
            fieldLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: labelInsets.right),
            fieldLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: labelInsets.bottom)
        ]
        NSLayoutConstraint.activate(stackViewConstraints)
    }
    
}
