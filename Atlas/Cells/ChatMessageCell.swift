//
//  ChatMessageCell.swift
//  Atlas
//
//  Created by Tim Park on 1/16/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import UIKit
import Firebase

class ChatMessageCell: UITableViewCell {
    
    let defaults = UserDefaults.standard
    
    let messageLabel: UILabel = {
        let messageLabel = UILabel()
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.lineBreakMode = .byWordWrapping
        messageLabel.numberOfLines = 0
        return messageLabel
    }()
    
    let timeStampLabel: UILabel = {
        let timeStampLabel = UILabel()
        timeStampLabel.translatesAutoresizingMaskIntoConstraints = false
        timeStampLabel.numberOfLines = 1
        timeStampLabel.font = .systemFont(ofSize: 12, weight: .medium)
        return timeStampLabel
    }()
    
    let bubbleBackgroundView: UIView = {
        let bubbleBackgroundView = UIView()
        bubbleBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        bubbleBackgroundView.layer.cornerRadius = 16
        return bubbleBackgroundView
    }()
    
    var leadingConstraint: NSLayoutConstraint!
    var trailingConstraint: NSLayoutConstraint!
    var timeStampLeadingConstraint: NSLayoutConstraint!
    var timeStampTrailingConstraint: NSLayoutConstraint!
    
    var chatMessage: ChatMessage! {
        didSet {
            if let user = Auth.auth().currentUser {
                // user is researcher
                if chatMessage.sender == user.email {
                    userSentSetup()
                } else {
                    userReceivedSetup()
                }
            } else {
                // user is patient
                if chatMessage.sender == defaults.string(forKey: "PATIENT_ID") {
                    userSentSetup()
                } else {
                    userReceivedSetup()
                }
            }
            messageLabel.text = chatMessage.text
            timeStampLabel.text = DateFormatter.sharedFormatter.string(from: chatMessage.date)
        }
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        backgroundColor = .systemBackground
        addSubview(bubbleBackgroundView)
        addSubview(messageLabel)
        addSubview(timeStampLabel)
        
        // lets set up some constraints for our label
        let constraints = [
            // default: 18, -18
            messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: 14),
            messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            messageLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 275),
            
            // default: 10, -12, 10, 12
            bubbleBackgroundView.topAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -12),
            bubbleBackgroundView.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -12),
            bubbleBackgroundView.bottomAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 10),
            bubbleBackgroundView.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 12),
            
            timeStampLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
        ]
        NSLayoutConstraint.activate(constraints)
        
        leadingConstraint = bubbleBackgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10)
        leadingConstraint.isActive = false
        
        trailingConstraint = bubbleBackgroundView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10)
        trailingConstraint.isActive = true
        
        timeStampLeadingConstraint = timeStampLabel.leadingAnchor.constraint(equalTo: bubbleBackgroundView.trailingAnchor, constant: 5)
        timeStampLeadingConstraint.isActive = false
        
        timeStampTrailingConstraint = timeStampLabel.trailingAnchor.constraint(equalTo: bubbleBackgroundView.leadingAnchor, constant: -5)
        timeStampTrailingConstraint.isActive = true
    }
    
    func userSentSetup() {
        // user sent the message
        bubbleBackgroundView.backgroundColor = .systemBlue
        messageLabel.textColor = .white
        leadingConstraint.isActive = false
        trailingConstraint.isActive = true
        timeStampLeadingConstraint.isActive = false
        timeStampTrailingConstraint.isActive = true
    }
    
    func userReceivedSetup() {
        // user received the message
        bubbleBackgroundView.backgroundColor = .systemGray4
        messageLabel.textColor = .black
        leadingConstraint.isActive = true
        trailingConstraint.isActive = false
        timeStampLeadingConstraint.isActive = true
        timeStampTrailingConstraint.isActive = false
    }
    
}
