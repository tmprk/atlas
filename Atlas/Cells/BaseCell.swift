//
//  BaseCell.swift
//  Atlas
//
//  Created by Tim Park on 10/2/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

class BaseCell: UITableViewCell {
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame = newFrame
            let newWidth = frame.width * 0.92 // get 80% width here
            let space = (frame.width - newWidth) / 2
            frame.size.width = newWidth
            frame.origin.x += space
            super.frame = frame
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func commonInit() {
        let backgroundView = UIView()
        backgroundView.backgroundColor = .systemBlue
        self.selectedBackgroundView = backgroundView
    }
    
}
