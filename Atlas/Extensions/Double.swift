//
//  Double.swift
//  Atlas
//
//  Created by Timothy Park on 8/14/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import Foundation
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
