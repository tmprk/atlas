//
//  Date+String.swift
//  Atlas
//
//  Created by Tim Park on 11/11/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import Foundation

extension Date {
    
    @nonobjc static var localFormatter: DateFormatter = {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateStyle = .medium
        dateStringFormatter.timeStyle = .medium
        dateStringFormatter.amSymbol = "AM"
        dateStringFormatter.pmSymbol = "PM"
        dateStringFormatter.dateFormat = "hh:mm a 'on' MMM dd, yyyy" //this your string date format
        dateStringFormatter.timeZone = .current
        dateStringFormatter.locale = .current
        return dateStringFormatter
    }()
    
    func localDateString() -> String {
        return Date.localFormatter.string(from: self)
    }
    
    // For Chat Messages
    static func dateFromCustomString(customString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yy h:mm a"
        return dateFormatter.date(from: customString) ?? Date()
    }
    
    func fromCustomFormat(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    static func relativeString(date: Date) -> String {
        let df = DateFormatter()
        df.timeStyle = .none
        df.dateStyle = .short
        df.doesRelativeDateFormatting = true
        return df.string(from: date)
    }
    
    func reduceToMonthDayYear() -> Date {
        let calendar = Calendar.current
        let month = calendar.component(.month, from: self)
        let day = calendar.component(.day, from: self)
        let year = calendar.component(.year, from: self)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yy"
        return dateFormatter.date(from: "\(month)/\(day)/\(year)") ?? Date()
    }
    
}

