//
//  UISplitViewController.swift
//  Atlas
//
//  Created by Timothy Park on 4/27/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import UIKit

extension UISplitViewController {
    
    func toggleMasterView() {
        if UIScreen.main.bounds.height > UIScreen.main.bounds.width {
            var nextDisplayMode: UISplitViewController.DisplayMode
            switch(self.preferredDisplayMode){
            case .primaryHidden:
                nextDisplayMode = .allVisible
            default:
                nextDisplayMode = .primaryHidden
            }
            UIView.animate(withDuration: 0.2) { () -> Void in
                self.preferredDisplayMode = nextDisplayMode
            }
        } else {
            // do nothing
        }
    }
    
}
