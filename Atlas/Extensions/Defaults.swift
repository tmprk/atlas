//
//  Defaults.swift
//  Atlas
//
//  Created by Timothy Park on 8/14/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import Foundation

enum GlobalSettings {
    @Property(key: "SurveyIndex", defaultValue: 0)
    static var surveyIndex: Int
}

@propertyWrapper
struct Property<T> {
    
    let key: String
    let defaultValue: T
    
    var wrappedValue: T {
        get {
            return UserDefaults.standard.object(forKey: key) as? T ?? defaultValue
        }
        set {
            UserDefaults.standard.set(newValue, forKey: key)
        }
    }
    
}
