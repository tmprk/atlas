//
//  UIColor+Theme.swift
//  Atlas
//
//  Created by Tim Park on 10/25/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var background: UIColor {
        return UIColor(red: 239/250, green: 239/250, blue: 244/250, alpha: 1)
    }
    
    static var waterMelon: UIColor {
        return UIColor(red:0.97, green:0.38, blue:0.45, alpha:1.00)
    }
    
    func mixLighter(amount: CGFloat = 0.25) -> UIColor {
        return mixWithColor(UIColor.white, amount:amount)
    }
    
    func mixDarker(amount: CGFloat = 0.25) -> UIColor {
        return mixWithColor(UIColor.black, amount:amount)
    }
    
    func mixWithColor(_ color: UIColor, amount: CGFloat = 0.25) -> UIColor {
        var r1     : CGFloat = 0
        var g1     : CGFloat = 0
        var b1     : CGFloat = 0
        var alpha1 : CGFloat = 0
        var r2     : CGFloat = 0
        var g2     : CGFloat = 0
        var b2     : CGFloat = 0
        var alpha2 : CGFloat = 0
        
        self.getRed (&r1, green: &g1, blue: &b1, alpha: &alpha1)
        color.getRed(&r2, green: &g2, blue: &b2, alpha: &alpha2)
        return UIColor( red:r1*(1.0-amount)+r2*amount,
                        green:g1*(1.0-amount)+g2*amount,
                        blue:b1*(1.0-amount)+b2*amount,
                        alpha: alpha1 )
    }
    
}
