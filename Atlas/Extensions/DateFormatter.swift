//
//  DateFormatter.swift
//  Atlas
//
//  Created by Tim Park on 1/18/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import Foundation

extension DateFormatter {

    static var sharedFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        // Add your formatter configuration here
        dateFormatter.dateFormat = "h:mm"
        return dateFormatter
    }()
    
}
