//
//  Dictionary.swift
//  Atlas
//
//  Created by Timothy Park on 7/8/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import Foundation

extension Dictionary {
    init(elements:[(Key, Value)]) {
        self.init()
        for (key, value) in elements {
            updateValue(value, forKey: key)
        }
    }
}
