//
//  Alertable.swift
//  Atlas
//
//  Created by Timothy Park on 7/12/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import UIKit

protocol Alertable { }
extension Alertable where Self: UIViewController {
    
    func showAlert(title: String, message: String, _ completion: @escaping(Bool)->(Void)) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        // let laterAction = UIAlertAction(title: "Later", style: .default) { action in
        //     completion(false)
        // }
        // alertController.addAction(laterAction)
        let okAction = UIAlertAction(title: "OK", style: .default) { action in
            completion(true)
        }
        alertController.addAction(okAction)
        DispatchQueue.main.async {
            self.view?.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showAlertWithSettings(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Take Me There", style: .default) { _ in
            DispatchQueue.main.async {
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            }
        }
        alertController.addAction(settingsAction)
        DispatchQueue.main.async {
            self.view?.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
}
