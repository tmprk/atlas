//
//  FollowUp.swift
//  Atlas
//
//  Created by Tim Park on 11/9/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import ResearchKit

struct FollowUpBuilder {
    
    var title: String!
    var timeFrame: String!
    
    public var FollowUp: ORKTask {
        var steps = [ORKStep]()
        
        let instructionStep = ORKInstructionStep(identifier: "IntroStep")
        if let title = self.title {
            instructionStep.title = "\(title)"
        }
        instructionStep.text = "This is a follow up"
        steps += [instructionStep]
        
        let question0: ORKScaleAnswerFormat = ORKScaleAnswerFormat(maximumValue: 10, minimumValue: 0, defaultValue: 5, step: 1, vertical: false, maximumValueDescription: "Worst Pain", minimumValueDescription: "No Pain")
        let question0Step = ORKQuestionStep(identifier: "0", title: nil, question: "In the past " + (timeFrame ?? "interval") + ", how would you rate your joint pain on average?", answer: question0)
        question0Step.isOptional = true
        steps += [question0Step]
        
        return ORKOrderedTask(identifier: "Follow Up", steps: steps)
    }
    
}
