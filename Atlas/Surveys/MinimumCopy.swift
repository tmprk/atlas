//
//  MinimumCopy.swift
//  Atlas
//
//  Created by Tim Park on 1/10/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import ResearchKit

public var MinimumCopy: ORKTask {
    
    var steps = [ORKStep]()
    
    let instructionStep = ORKInstructionStep(identifier: "IntroStep")
    instructionStep.title = "NIH Minimum Data Set - Initial"
    instructionStep.text = "We'll ask you a few questions..."
    steps += [instructionStep]

    let question3: ORKScaleAnswerFormat = ORKAnswerFormat.scale(withMaximumValue: 10, minimumValue: 0, defaultValue: 5, step: 1, vertical: false, maximumValueDescription: "Worst Imaginable Pain", minimumValueDescription: "No Pain")
    let question3Step = ORKQuestionStep(identifier: "3", title: nil, question: "In the past 7 days, how would you rate your low back pain on average?", answer: question3)
    question3Step.isOptional = true
    steps += [question3Step]

    let question4Choices = [
        ORKTextChoice(text: "Yes", value: "Yes" as NSString),
        ORKTextChoice(text: "No", value: "No" as NSString),
        ORKTextChoice(text: "Not Sure", value: "Not Sure" as NSString)
    ]
    let question4: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    let question4Step = ORKQuestionStep(identifier: "4", title: nil, question: "Has back pain spread down your leg(s) during the past 2 weeks?", answer: question4)
    question4Step.isOptional = true
    steps += [question4Step]



    let question5Choices = [
        ORKTextChoice(text: "Not bothered at all", value: "Not bothered at all" as NSString),
        ORKTextChoice(text: "bothered a little", value: "bothered a little" as NSString),
        ORKTextChoice(text: "bothered a lot", value: "bothered a lot" as NSString)
    ]
    let question5: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question5Choices)
    let question5Step = ORKQuestionStep(identifier: "5", title: nil, question: "During the past 4 weeks, how much have you been bothered by stomach pain?", answer: question5)
    question5Step.isOptional = true
    steps += [question5Step]

    let question6: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question5Choices)
    let question6Step = ORKQuestionStep(identifier: "6", title: nil, question: "During the past 4 weeks, how much have you been bothered by pain in your arms, legs, or joints other than your spine or back?", answer: question6)
    question6Step.isOptional = true
    steps += [question6Step]

    let question7: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question5Choices)
    let question7Step = ORKQuestionStep(identifier: "7", title: nil, question: "During the past 4 weeks, how much have you been bothered by headaches?", answer: question7)
    question7Step.isOptional = true
    steps += [question7Step]


    let question8: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question5Choices)
    let question8Step = ORKQuestionStep(identifier: "8", title: nil, question: "During the past 4 weeks, how much have you been bothered by widespread pain or pain in most of your body?", answer: question8)
    question8Step.isOptional = true
    steps += [question8Step]


    let question9: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question5Choices)
    let question9Step = ORKQuestionStep(identifier: "9", title: nil, question: "the past 4 weeks, how much have you been bothered by widespread pain or pain in most of your body?", answer: question9)
    question9Step.isOptional = true
    steps += [question9Step]

    let question13Choices = [
        ORKTextChoice(text: "Not at all", value: "Not at all" as NSString),
        ORKTextChoice(text: "A little bit", value: "A little bit" as NSString),
        ORKTextChoice(text: "Somewhat", value: "Somewhat" as NSString),
        ORKTextChoice(text: "Quite a bit", value: "Quite a bit" as NSString),
        ORKTextChoice(text: "Very much", value: "Very much" as NSString)
    ]
    let question13: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    let question13Step = ORKQuestionStep(identifier: "13", title: nil, question: "In the past 7 days, how much did pain interfere with your day-to-day activities?", answer: question13)
    question13Step.isOptional = true
    steps += [question13Step]

    let question14: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    let question14Step = ORKQuestionStep(identifier: "14", title: nil, question: "In the past 7 days, how much did pain interfere with work around the home?", answer: question14)
    question14Step.isOptional = true
    steps += [question14Step]

    let question15: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    let question15Step = ORKQuestionStep(identifier: "15", title: nil, question: "In the past 7 days, how much did pain interfere with your ability to participate in social activities?", answer: question15)
    question15Step.isOptional = true
    steps += [question15Step]

    let question16: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    let question16Step = ORKQuestionStep(identifier: "16", title: nil, question: "In the past 7 days, how much did pain interfere with your household chores?", answer: question16)
    question16Step.isOptional = true
    steps += [question16Step]

    let question17: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    let question17Step = ORKQuestionStep(identifier: "17", title: nil, question: "Have you used opioid painkillers for your back pain?", answer: question17)
    question17Step.isOptional = true
    steps += [question17Step]

    let question18Choices = [
        ORKTextChoice(text: "Yes", value: "Yes" as NSString),
        ORKTextChoice(text: "No", value: "No" as NSString)
    ]
    let question18: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question18Choices)
    let question18Step = ORKQuestionStep(identifier: "18", title: nil, question: "If you checked yes to having used opioid painkillers, are you currently using this medication?", answer: question18)
    question18Step.isOptional = true
    steps += [question18Step]


    let question19: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    let question19Step = ORKQuestionStep(identifier: "19", title: nil, question: "Have you used injections (such as epidural steroid injections, facet injections)?", answer: question19)
    question19Step.isOptional = true
    steps += [question19Step]

    let question20: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    let question20Step = ORKQuestionStep(identifier: "20", title: nil, question: "Have you used exercise therapy?", answer: question20)
    question20Step.isOptional = true
    steps += [question20Step]

    let question21: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    let question21Step = ORKQuestionStep(identifier: "21", title: nil, question: "Have you used psychological counseling, such as cognitive-behavioral therapy?", answer: question21)
    question21Step.isOptional = true
    steps += [question21Step]


    let question22Choices = [
        ORKTextChoice(text: "Agree", value: "Agree" as NSString),
        ORKTextChoice(text: "Disagree", value: "Disagree" as NSString),
        ORKTextChoice(text: "Does not apply", value: "Does not apply" as NSString)
    ]
    let question22: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question22Choices)
    let question22Step = ORKQuestionStep(identifier: "22", title: nil, question: "I have been off work or unemployed for 1 month or more due to low-back pain.", answer: question22)
    question22Step.isOptional = true
    steps += [question22Step]

    let question23: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question22Choices)
    let question23Step = ORKQuestionStep(identifier: "23", title: nil, question: "I receive or have applied for disability or worker’s compensation benefits because I am unable to work due to low-back pain.", answer: question23)
    question23Step.isOptional = true
    steps += [question23Step]

    let question24Choices = [
        ORKTextChoice(text: "Without any difficulty", value: "Without any difficulty" as NSString),
        ORKTextChoice(text: "With a little difficulty", value: "With a little difficulty" as NSString),
        ORKTextChoice(text: "With some difficulty", value: "With some difficulty" as NSString),
        ORKTextChoice(text: "With much difficulty", value: "With much difficulty" as NSString),
        ORKTextChoice(text: "Unable to do", value: "Unable to do" as NSString)
    ]
    let question24: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question24Choices)
    let question24Step = ORKQuestionStep(identifier: "24", title: nil, question: "Are you able to do chores such as vacuuming or yard work?", answer: question24)
    question24Step.isOptional = true
    steps += [question24Step]

    let question25: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question24Choices)
    let question25Step = ORKQuestionStep(identifier: "25", title: nil, question: "Are you able to go up and down stairs at a normal pace?", answer: question25)
    question25Step.isOptional = true
    steps += [question25Step]

    let question26: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question24Choices)
    let question26Step = ORKQuestionStep(identifier: "26", title: nil, question: "Are you able to go for a walk of at least 15 minutes?", answer: question26)
    question26Step.isOptional = true
    steps += [question26Step]

    let question27: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question24Choices)
    let question27Step = ORKQuestionStep(identifier: "27", title: nil, question: "Are you able to run errands or shop?", answer: question27)
    question27Step.isOptional = true
    steps += [question27Step]

    let question28Choices = [
        ORKTextChoice(text: "Never", value: "Never" as NSString),
        ORKTextChoice(text: "Rarely", value: "Rarely" as NSString),
        ORKTextChoice(text: "Sometimes", value: "Sometimes" as NSString),
        ORKTextChoice(text: "Often", value: "Often" as NSString),
        ORKTextChoice(text: "Always", value: "Always" as NSString)
    ]
    let question28: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question28Choices)
    let question28Step = ORKQuestionStep(identifier: "28", title: nil, question: "In the past 7 days, I felt worthless.", answer: question28)
    question28Step.isOptional = true
    steps += [question28Step]

    let question29: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question28Choices)
    let question29Step = ORKQuestionStep(identifier: "29", title: nil, question: "In the past 7 days, I felt depressed.", answer: question29)
    question29Step.isOptional = true
    steps += [question29Step]

    let question30: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question28Choices)
    let question30Step = ORKQuestionStep(identifier: "30", title: nil, question: "In the past 7 days, I felt hopeless.", answer: question30)
    question30Step.isOptional = true
    steps += [question30Step]

    let question31Choices = [
        ORKTextChoice(text: "Very poor", value: "Very poor" as NSString),
        ORKTextChoice(text: "Poor", value: "Poor" as NSString),
        ORKTextChoice(text: "Fair", value: "Fair" as NSString),
        ORKTextChoice(text: "Good", value: "Good" as NSString),
        ORKTextChoice(text: "Very good", value: "Very good" as NSString)
    ]
    let question31: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question31Choices)
    let question31Step = ORKQuestionStep(identifier: "31", title: nil, question: "In the past 7 days, my sleep quality was...", answer: question31)
    question31Step.isOptional = true
    steps += [question31Step]

    let question32: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    let question32Step = ORKQuestionStep(identifier: "32", title: nil, question: "In the past 7 days, my sleep was refreshing.", answer: question32)
    question32Step.isOptional = true
    steps += [question32Step]

    let question33: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    let question33Step = ORKQuestionStep(identifier: "33", title: nil, question: "In the past 7 days, I had a problem with my sleep.", answer: question33)
    question33Step.isOptional = true
    steps += [question33Step]

    let question34: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    let question34Step = ORKQuestionStep(identifier: "34", title: nil, question: "In the past 7 days, I had difficulty falling asleep.", answer: question34)
    question34Step.isOptional = true
    steps += [question34Step]

    let question35Choices = [
        ORKTextChoice(text: "Agree", value: "Agree" as NSString),
        ORKTextChoice(text: "Disagree", value: "Disagree" as NSString)
    ]
    let question35: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question35Choices)
    let question35Step = ORKQuestionStep(identifier: "35", title: nil, question: "It’s not really safe for a person with my back problem to be physically active.", answer: question35)
    question35Step.isOptional = true
    steps += [question35Step]

    let question36: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question35Choices)
    let question36Step = ORKQuestionStep(identifier: "36", title: nil, question: "I feel that my back pain is terrible and it’s never going to get any better.", answer: question36)
    question36Step.isOptional = true
    steps += [question36Step]

    let question37: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    let question37Step = ORKQuestionStep(identifier: "37", title: nil, question: "Are you involved in a lawsuit or legal claim related to your back problem?", answer: question37)
    question37Step.isOptional = true
    steps += [question37Step]

    let question38: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question28Choices)
    let question38Step = ORKQuestionStep(identifier: "38", title: nil, question: "In the past year, have you drunk or used drugs more than you meant to?", answer: question38)
    question38Step.isOptional = true
    steps += [question38Step]

    let question39: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question28Choices)
    let question39Step = ORKQuestionStep(identifier: "39", title: nil, question: "In the past year, have you felt you wanted or needed to cut down on your drinking or drug use?", answer: question39)
    question39Step.isOptional = true
    steps += [question39Step]
    
    let question44Choices = [
        ORKTextChoice(text: "Working now", value: "Working now" as NSString),
        ORKTextChoice(text: "Looking for work", value: "Looking for work" as NSString),
        ORKTextChoice(text: "Unemployed", value: "Unemployed" as NSString),
        ORKTextChoice(text: "Sick leave or maternity leave", value: "Sick leave or maternity leave" as NSString),
        ORKTextChoice(text: "Disabled due to back pain, permanently or temporarily", value: "Disabled due to back pain, permanently or temporarily" as NSString),
        ORKTextChoice(text: "Disabled for reasons other than back pain", value: "Disabled for reasons other than back pain" as NSString),
        ORKTextChoice(text: "Student", value: "Student" as NSString),
        ORKTextChoice(text: "Temporarily laid off", value: "Temporarily laid off" as NSString),
        ORKTextChoice(text: "Retired", value: "Retired" as NSString),
        ORKTextChoice(text: "Keeping house", value: "Keeping house" as NSString),
        ORKTextChoice(text: "Other (specify)", value: "Other (specify)" as NSString),
        ORKTextChoice(text: "Unknown", value: "Unknown" as NSString),
    ]
    let question44: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question44Choices)
    let question44Step = ORKQuestionStep(identifier: "44", title: nil, question: "Employment Status:", answer: question44)
    question44Step.isOptional = true
    steps += [question44Step]
    
    let question45: ORKTextAnswerFormat = ORKAnswerFormat.textAnswerFormat(withMaximumLength: 100)
    let question45Step = ORKQuestionStep(identifier: "45", title: nil, question: "Specify employment status", answer: question45)
    question45Step.isOptional = true
    steps += [question45Step]
    
    let predicate44to46 = ORKResultPredicate.predicateForChoiceQuestionResult(with: ORKResultSelector(resultIdentifier: question44Step.identifier), expectedAnswerValue: "Other (specify)" as NSString)
    let rule44to46 = ORKPredicateSkipStepNavigationRule(resultPredicate: predicate44to46)
    
    let question47Choices = [
        ORKTextChoice(text: "Never smoked", value: "Never smoked" as NSString),
        ORKTextChoice(text: "Current smoker", value: "Current smoker" as NSString),
        ORKTextChoice(text: "Used to smoke, but have now quit", value: "Used to smoke, but have now quit" as NSString)
    ]
    let question47: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question47Choices)
    let question47Step = ORKQuestionStep(identifier: "47", title: nil, question: "How would you describe your cigarette smoking?", answer: question47)
    question47Step.isOptional = true
    steps += [question47Step]
    
    let question48: ORKHeightAnswerFormat = ORKAnswerFormat.heightAnswerFormat(with: .local)
    let question48step = ORKQuestionStep(identifier: "48", title: nil, question: "What is your height?", answer: question48)
    question48step.isOptional = true
    steps += [question48step]
    
    let question49: ORKWeightAnswerFormat = ORKAnswerFormat.weightAnswerFormat(with: .local, numericPrecision: .default)
    let question49step = ORKQuestionStep(identifier: "49", title: nil, question: "What is your weight?", answer: question49)
    question49step.isOptional = true
    steps += [question49step]
    
    let summaryStep = ORKCompletionStep(identifier: "SummaryStep")
    summaryStep.title = "Right. Off you go!"
    summaryStep.text = "That was easy!"
    steps += [summaryStep]
    
    let task = ORKNavigableOrderedTask(identifier: "Minimum Dataset", steps: steps)
    task.setSkip(rule44to46, forStepIdentifier: question45Step.identifier)
    task.shouldReportProgress = true
    
    // return ORKOrderedTask(identifier: "SurveyTask", steps: steps)
    return task
}
