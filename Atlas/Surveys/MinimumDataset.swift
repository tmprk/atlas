//
//  MinimumDataset.swift
//  Atlas
//
//  Created by Tim Park on 10/25/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import ResearchKit

public var MinimumDataset: ORKTask {
    
    var steps = [ORKStep]()
    
    let instructionStep = ORKInstructionStep(identifier: "IntroStep")
    instructionStep.title = "NIH Minimum Data Set - Initial"
    instructionStep.text = "We'll ask you a few questions..."
    steps += [instructionStep]
    
    let question1Choices = [
        ORKTextChoice(text: "Less than 1 month", value: "Less than 1 month" as NSString),
        ORKTextChoice(text: "1 - 3 months", value: "1 - 3 months" as NSString),
        ORKTextChoice(text: "3 - 6 months", value: "3 - 6 months" as NSString),
        ORKTextChoice(text: "6 months - 1 Year", value: "6 months - 1 Year" as NSString),
        ORKTextChoice(text: "1 - 5 Years", value: "1 - 5 Years" as NSString),
        ORKTextChoice(text: "More than 5 Years", value: "More than 5 Years" as NSString),
    ]
    let question1: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question1Choices)
    let question1Step = ORKQuestionStep(identifier: "1", title: nil, question: "How long has low back pain been an ongoing problem for you?", answer: question1)
    question1Step.isOptional = true
    steps += [question1Step]
    
    let question2Choices = [
        ORKTextChoice(text: "Every day or nearly every day in the past 6 months", value: "Every day or nearly every day in the past 6 months" as NSString),
        ORKTextChoice(text: "At least half the days in the past 6 months", value: "At least half the days in the past 6 months" as NSString),
        ORKTextChoice(text: "Less than half the days in the past 6 months", value: "Less than half the days in the past 6 months" as NSString)
    ]
    let question2: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question2Choices)
    let quesiton2Step = ORKQuestionStep(identifier: "2", title: nil, question: "How often has low back pain been an ongoing problem for you over the past 6 months?", answer: question2)
    quesiton2Step.isOptional = true
    steps += [quesiton2Step]
    
    let question3: ORKScaleAnswerFormat = ORKAnswerFormat.scale(withMaximumValue: 10, minimumValue: 0, defaultValue: 5, step: 1, vertical: false, maximumValueDescription: "Worst Imaginable Pain", minimumValueDescription: "No Pain")
    let question3Step = ORKQuestionStep(identifier: "3", title: nil, question: "In the past 7 days, how would you rate your joint pain on average?", answer: question3)
    question3Step.isOptional = true
    steps += [question3Step]
    
    let question4Choices = [
        ORKTextChoice(text: "Yes", value: "Yes" as NSString),
        ORKTextChoice(text: "No", value: "No" as NSString),
        ORKTextChoice(text: "Not Sure", value: "Not Sure" as NSString)
    ]
    let question4: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    let question4Step = ORKQuestionStep(identifier: "4", title: nil, question: "Has back pain spread down your leg(s) during the past 2 weeks?", answer: question4)
    question4Step.isOptional = true
    steps += [question4Step]
    
    // let question5Choices = [
    //     ORKTextChoice(text: "Not bothered at all", value: "Not bothered at all" as NSString),
    //     ORKTextChoice(text: "Bothered a little", value: "Bothered a little" as NSString),
    //     ORKTextChoice(text: "Bothered a lot", value: "Bothered a lot" as NSString)
    // ]
    // let question5: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question5Choices)
    // let question5Step = ORKQuestionStep(identifier: "5", title: nil, question: "During the past 4 weeks, how much have you been bothered by stomach pain?", answer: question5)
    // question5Step.isOptional = true
    // steps += [question5Step]
    //
    //
    // let question6: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question5Choices)
    // let question6Step = ORKQuestionStep(identifier: "6", title: nil, question: "During the past 4 weeks, how much have you been bothered by pain in your arms, legs, or joints other than your spine or back?", answer: question6)
    // question6Step.isOptional = true
    // steps += [question6Step]
    //
    //
    // let question7: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question5Choices)
    // let question7Step = ORKQuestionStep(identifier: "7", title: nil, question: "During the past 4 weeks, how much have you been bothered by headaches?", answer: question7)
    // question7Step.isOptional = true
    // steps += [question7Step]
    //
    //
    // let question8: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question5Choices)
    // let question8Step = ORKQuestionStep(identifier: "8", title: nil, question: "During the past 4 weeks, how much have you been bothered by widespread pain or pain in most of your body?", answer: question8)
    // question8Step.isOptional = true
    // steps += [question8Step]
    //
    // // deleted number 9 duplicate of 8
    // let question10Choices = [
    //     ORKTextChoice(text: "Yes, one operation", value: "Yes, one operation" as NSString),
    //     ORKTextChoice(text: "Yes, more than one operation", value: "Yes, more than one operation" as NSString),
    //     ORKTextChoice(text: "No", value: "No" as NSString)
    // ]
    // let question10: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question10Choices)
    // let question10Step = ORKQuestionStep(identifier: "10", title: nil, question: "Have you ever had a low back operation?", answer: question10)
    // question10Step.isOptional = true
    // steps += [question10Step]
    //
    // let question11Choices = [
    //     ORKTextChoice(text: "Less than 6 months ago", value: "Less than 6 months ago" as NSString),
    //     ORKTextChoice(text: "More than 6 months ago but less than 1 year ago", value: "More than 6 months ago but less than 1 year ago" as NSString),
    //     ORKTextChoice(text: "Between 1 and 2 years ago", value: "Between 1 and 2 years ago" as NSString),
    //     ORKTextChoice(text: "More than 2 years ago", value: "More than 2 years ago" as NSString)
    // ]
    // let question11: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question11Choices)
    // let question11Step = ORKQuestionStep(identifier: "11", title: nil, question: "If yes, when was your last back operation?", answer: question11)
    // question11Step.isOptional = true
    // steps += [question11Step]
    //
    // let question12: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    // let question12Step = ORKQuestionStep(identifier: "12", title: nil, question: "Did any of your back operations involve a spinal fusion? (also called an arthrodesis)", answer: question12)
    // question12Step.isOptional = true
    // steps += [question12Step]
    //
    // let question13Choices = [
    //     ORKTextChoice(text: "Not at all", value: "Not at all" as NSString),
    //     ORKTextChoice(text: "A little bit", value: "A little bit" as NSString),
    //     ORKTextChoice(text: "Somewhat", value: "Somewhat" as NSString),
    //     ORKTextChoice(text: "Quite a bit", value: "Quite a bit" as NSString),
    //     ORKTextChoice(text: "Very much", value: "Very much" as NSString)
    // ]
    // let question13: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    // let question13Step = ORKQuestionStep(identifier: "13", title: nil, question: "In the past 7 days, how much did pain interfere with your day-to-day activities?", answer: question13)
    // question13Step.isOptional = true
    // steps += [question13Step]
    //
    // let predicate10to12 = ORKResultPredicate.predicateForChoiceQuestionResult(with: ORKResultSelector(resultIdentifier: question10Step.identifier), matchingPattern: "No")
    // let rule10to12 = ORKPredicateStepNavigationRule(resultPredicatesAndDestinationStepIdentifiers: [(predicate10to12, question12Step.identifier)])
    //
    // let question14: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    // let question14Step = ORKQuestionStep(identifier: "14", title: nil, question: "In the past 7 days, how much did pain interfere with work around the home?", answer: question14)
    // question14Step.isOptional = true
    // steps += [question14Step]
    //
    // let question15: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    // let question15Step = ORKQuestionStep(identifier: "15", title: nil, question: "In the past 7 days, how much did pain interfere with your ability to participate in social activities?", answer: question15)
    // question15Step.isOptional = true
    // steps += [question15Step]
    //
    // let question16: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    // let question16Step = ORKQuestionStep(identifier: "16", title: nil, question: "In the past 7 days, how much did pain interfere with your household chores?", answer: question16)
    // question16Step.isOptional = true
    // steps += [question16Step]
    //
    // let question17: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    // let question17Step = ORKQuestionStep(identifier: "17", title: nil, question: "Have you used opioid painkillers for your back pain?", answer: question17)
    // question17Step.isOptional = true
    // steps += [question17Step]
    //
    // let question18Choices = [
    //     ORKTextChoice(text: "Yes", value: "Yes" as NSString),
    //     ORKTextChoice(text: "No", value: "No" as NSString)
    // ]
    // let question18: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question18Choices)
    // let question18Step = ORKQuestionStep(identifier: "18", title: nil, question: "If you checked yes to having used opioid painkillers, are you currently using this medication?", answer: question18)
    // question18Step.isOptional = true
    // steps += [question18Step]
    //
    //
    // let question19: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    // let question19Step = ORKQuestionStep(identifier: "19", title: nil, question: "Have you used injections (such as epidural steroid injections, facet injections)?", answer: question19)
    // question19Step.isOptional = true
    // steps += [question19Step]
    //
    // let question20: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    // let question20Step = ORKQuestionStep(identifier: "20", title: nil, question: "Have you used exercise therapy?", answer: question20)
    // question20Step.isOptional = true
    // steps += [question20Step]
    //
    // let question21: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    // let question21Step = ORKQuestionStep(identifier: "21", title: nil, question: "Have you used psychological counseling, such as cognitive-behavioral therapy?", answer: question21)
    // question21Step.isOptional = true
    // steps += [question21Step]
    //
    //
    // let question22Choices = [
    //     ORKTextChoice(text: "Agree", value: "Agree" as NSString),
    //     ORKTextChoice(text: "Disagree", value: "Disagree" as NSString),
    //     ORKTextChoice(text: "Does not apply", value: "Does not apply" as NSString)
    // ]
    // let question22: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question22Choices)
    // let question22Step = ORKQuestionStep(identifier: "22", title: nil, question: "I have been off work or unemployed for 1 month or more due to low-back pain.", answer: question22)
    // question22Step.isOptional = true
    // steps += [question22Step]
    //
    // let question23: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question22Choices)
    // let question23Step = ORKQuestionStep(identifier: "23", title: nil, question: "I receive or have applied for disability or worker’s compensation benefits because I am unable to work due to low-back pain.", answer: question23)
    // question23Step.isOptional = true
    // steps += [question23Step]
    //
    // let question24Choices = [
    //     ORKTextChoice(text: "Without any difficulty", value: "Without any difficulty" as NSString),
    //     ORKTextChoice(text: "With a little difficulty", value: "With a little difficulty" as NSString),
    //     ORKTextChoice(text: "With some difficulty", value: "With some difficulty" as NSString),
    //     ORKTextChoice(text: "With much difficulty", value: "With much difficulty" as NSString),
    //     ORKTextChoice(text: "Unable to do", value: "Unable to do" as NSString)
    // ]
    // let question24: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question24Choices)
    // let question24Step = ORKQuestionStep(identifier: "24", title: nil, question: "Are you able to do chores such as vacuuming or yard work?", answer: question24)
    // question24Step.isOptional = true
    // steps += [question24Step]
    //
    // let question25: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question24Choices)
    // let question25Step = ORKQuestionStep(identifier: "25", title: nil, question: "Are you able to go up and down stairs at a normal pace?", answer: question25)
    // question25Step.isOptional = true
    // steps += [question25Step]
    //
    // let question26: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question24Choices)
    // let question26Step = ORKQuestionStep(identifier: "26", title: nil, question: "Are you able to go for a walk of at least 15 minutes?", answer: question26)
    // question26Step.isOptional = true
    // steps += [question26Step]
    //
    // let question27: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question24Choices)
    // let question27Step = ORKQuestionStep(identifier: "27", title: nil, question: "Are you able to run errands or shop?", answer: question27)
    // question27Step.isOptional = true
    // steps += [question27Step]
    //
    // let question28Choices = [
    //     ORKTextChoice(text: "Never", value: "Never" as NSString),
    //     ORKTextChoice(text: "Rarely", value: "Rarely" as NSString),
    //     ORKTextChoice(text: "Sometimes", value: "Sometimes" as NSString),
    //     ORKTextChoice(text: "Often", value: "Often" as NSString),
    //     ORKTextChoice(text: "Always", value: "Always" as NSString)
    // ]
    // let question28: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question28Choices)
    // let question28Step = ORKQuestionStep(identifier: "28", title: nil, question: "In the past 7 days, I felt worthless.", answer: question28)
    // question28Step.isOptional = true
    // steps += [question28Step]
    //
    // let question29: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question28Choices)
    // let question29Step = ORKQuestionStep(identifier: "29", title: nil, question: "In the past 7 days, I felt depressed.", answer: question29)
    // question29Step.isOptional = true
    // steps += [question29Step]
    //
    // let question30: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question28Choices)
    // let question30Step = ORKQuestionStep(identifier: "30", title: nil, question: "In the past 7 days, I felt hopeless.", answer: question30)
    // question30Step.isOptional = true
    // steps += [question30Step]
    //
    // let question31Choices = [
    //     ORKTextChoice(text: "Very poor", value: "Very poor" as NSString),
    //     ORKTextChoice(text: "Poor", value: "Poor" as NSString),
    //     ORKTextChoice(text: "Fair", value: "Fair" as NSString),
    //     ORKTextChoice(text: "Good", value: "Good" as NSString),
    //     ORKTextChoice(text: "Very good", value: "Very good" as NSString)
    // ]
    // let question31: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question31Choices)
    // let question31Step = ORKQuestionStep(identifier: "31", title: nil, question: "In the past 7 days, my sleep quality was...", answer: question31)
    // question31Step.isOptional = true
    // steps += [question31Step]
    //
    // let question32: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    // let question32Step = ORKQuestionStep(identifier: "32", title: nil, question: "In the past 7 days, my sleep was refreshing.", answer: question32)
    // question32Step.isOptional = true
    // steps += [question32Step]
    //
    // let question33: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    // let question33Step = ORKQuestionStep(identifier: "33", title: nil, question: "In the past 7 days, I had a problem with my sleep.", answer: question33)
    // question33Step.isOptional = true
    // steps += [question33Step]
    //
    // let question34: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question13Choices)
    // let question34Step = ORKQuestionStep(identifier: "34", title: nil, question: "In the past 7 days, I had difficulty falling asleep.", answer: question34)
    // question34Step.isOptional = true
    // steps += [question34Step]
    //
    // let question35Choices = [
    //     ORKTextChoice(text: "Agree", value: "Agree" as NSString),
    //     ORKTextChoice(text: "Disagree", value: "Disagree" as NSString)
    // ]
    // let question35: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question35Choices)
    // let question35Step = ORKQuestionStep(identifier: "35", title: nil, question: "It’s not really safe for a person with my back problem to be physically active.", answer: question35)
    // question35Step.isOptional = true
    // steps += [question35Step]
    //
    // let question36: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question35Choices)
    // let question36Step = ORKQuestionStep(identifier: "36", title: nil, question: "I feel that my back pain is terrible and it’s never going to get any better.", answer: question36)
    // question36Step.isOptional = true
    // steps += [question36Step]
    //
    // let question37: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question4Choices)
    // let question37Step = ORKQuestionStep(identifier: "37", title: nil, question: "Are you involved in a lawsuit or legal claim related to your back problem?", answer: question37)
    // question37Step.isOptional = true
    // steps += [question37Step]
    //
    // let question38: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question28Choices)
    // let question38Step = ORKQuestionStep(identifier: "38", title: nil, question: "In the past year, have you drunk or used drugs more than you meant to?", answer: question38)
    // question38Step.isOptional = true
    // steps += [question38Step]
    //
    // let question39: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question28Choices)
    // let question39Step = ORKQuestionStep(identifier: "39", title: nil, question: "In the past year, have you felt you wanted or needed to cut down on your drinking or drug use?", answer: question39)
    // question39Step.isOptional = true
    // steps += [question39Step]
    //
    // let question40format = ORKNumericAnswerFormat.integerAnswerFormat(withUnit: "years")
    // question40format.minimum = 18
    // question40format.maximum = 100
    // let question40Step = ORKQuestionStep(identifier: "40", title: nil, question: "What is your current age?", answer: question40format)
    // question40Step.isOptional = true
    // steps += [question40Step]
    //
    // let question41Choices = [
    //     ORKTextChoice(text: "Female", value: "Female" as NSString),
    //     ORKTextChoice(text: "Male", value: "Male" as NSString),
    //     ORKTextChoice(text: "Unknown", value: "Unknown" as NSString),
    //     ORKTextChoice(text: "Unspecified", value: "Unspecified" as NSString)
    // ]
    // let question41: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question41Choices)
    // let question41Step = ORKQuestionStep(identifier: "41", title: nil, question: "Gender:", answer: question41)
    // question41Step.isOptional = true
    // steps += [question41Step]
    //
    // let question42Choices = [
    //     ORKTextChoice(text: "Hispanic or Latino", value: "Hispanic or Latino" as NSString),
    //     ORKTextChoice(text: "Not Hispanic or Latino", value: "Not Hispanic or Latino" as NSString),
    //     ORKTextChoice(text: "Unknown", value: "Unknown" as NSString),
    //     ORKTextChoice(text: "Not Reported", value: "Not Reported" as NSString)
    // ]
    // let question42: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question42Choices)
    // let question42Step = ORKQuestionStep(identifier: "42", title: nil, question: "Ethnicity: (Choose only one with which you MOST CLOSELY identify)", answer: question42)
    // question42Step.isOptional = true
    // steps += [question42Step]
    //
    //
    // let question43Choices = [
    //     ORKTextChoice(text: "American Indian or Alaska Native", value: "American Indian or Alaska Native" as NSString),
    //     ORKTextChoice(text: "Asian", value: "Asian" as NSString),
    //     ORKTextChoice(text: "Black or African-American", value: "Black or African-American" as NSString),
    //     ORKTextChoice(text: "Native Hawaiian or Other Pacific Islander", value: "Native Hawaiian or Other Pacific Islander" as NSString),
    //     ORKTextChoice(text: "White", value: "White" as NSString),
    //     ORKTextChoice(text: "Unknown", value: "Unknown" as NSString),
    //     ORKTextChoice(text: "Not Reported", value: "Not Reported" as NSString)
    // ]
    // let question43: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question43Choices)
    // let question43Step = ORKQuestionStep(identifier: "43", title: nil, question: "Race: (Choose only one with which you MOST CLOSELY identify)", answer: question43)
    // question43Step.isOptional = true
    // steps += [question43Step]
    //
    // // add (specify) to Other option
    // let question44Choices = [
    //     ORKTextChoice(text: "Working now", value: "Working now" as NSString),
    //     ORKTextChoice(text: "Looking for work", value: "Looking for work" as NSString),
    //     ORKTextChoice(text: "Unemployed", value: "Unemployed" as NSString),
    //     ORKTextChoice(text: "Sick leave or maternity leave", value: "Sick leave or maternity leave" as NSString),
    //     ORKTextChoice(text: "Disabled due to back pain, permanently or temporarily", value: "Disabled due to back pain, permanently or temporarily" as NSString),
    //     ORKTextChoice(text: "Disabled for reasons other than back pain", value: "Disabled for reasons other than back pain" as NSString),
    //     ORKTextChoice(text: "Student", value: "Student" as NSString),
    //     ORKTextChoice(text: "Temporarily laid off", value: "Temporarily laid off" as NSString),
    //     ORKTextChoice(text: "Retired", value: "Retired" as NSString),
    //     ORKTextChoice(text: "Keeping house", value: "Keeping house" as NSString),
    //     ORKTextChoice(text: "Other", value: "Other" as NSString),
    //     ORKTextChoice(text: "Unknown", value: "Unknown" as NSString),
    // ]
    // let question44: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question44Choices)
    // let question44Step = ORKQuestionStep(identifier: "44", title: nil, question: "Employment Status:", answer: question44)
    // question44Step.isOptional = true
    // steps += [question44Step]
    //
    // let question45: ORKTextAnswerFormat = ORKAnswerFormat.textAnswerFormat(withMaximumLength: 100)
    // let question45Step = ORKQuestionStep(identifier: "45", title: nil, question: "Specify employment status", answer: question45)
    // question45Step.isOptional = true
    // steps += [question45Step]
    //
    // let question46Choices = [
    //     ORKTextChoice(text: "No high school diploma", value: "No high school diploma" as NSString),
    //     ORKTextChoice(text: "High school graduate or GED", value: "High school graduate or GED" as NSString),
    //     ORKTextChoice(text: "Some college, no degree", value: "Some college, no degree" as NSString),
    //     ORKTextChoice(text: "Occupational/technical/vocational program", value: "Occupational/technical/vocational program" as NSString),
    //     ORKTextChoice(text: "Associate degree: academic program", value: "Associate degree: academic program" as NSString),
    //     ORKTextChoice(text: "Bachelor’s degree", value: "Bachelor’s degree" as NSString),
    //     ORKTextChoice(text: "Master’s degree (e.g., M.A., M.S., M. Eng., M. ED., M.B.A)", value: "Master’s degree (e.g., M.A., M.S., M. Eng., M. ED., M.B.A)" as NSString),
    //     ORKTextChoice(text: "Professional school degree (e.g., M.D., D.D.S., D.V.M., J.D.)", value: "Professional school degree (e.g., M.D., D.D.S., D.V.M., J.D.)" as NSString),
    //     ORKTextChoice(text: "Doctoral degree (e.g., Ph.D., Ed.D.)", value: "Doctoral degree (e.g., Ph.D., Ed.D.)" as NSString),
    //     ORKTextChoice(text: "Unknown", value: "Unknown" as NSString),
    // ]
    // let question46: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question46Choices)
    // let question46Step = ORKQuestionStep(identifier: "46", title: nil, question: "Education Level: (select the highest level attained)", answer: question46)
    // question46Step.isOptional = true
    // steps += [question46Step]
    //
    // let question47Choices = [
    //     ORKTextChoice(text: "Never smoked", value: "Never smoked" as NSString),
    //     ORKTextChoice(text: "Current smoker", value: "Current smoker" as NSString),
    //     ORKTextChoice(text: "Used to smoke, but have now quit", value: "Used to smoke, but have now quit" as NSString)
    // ]
    // let question47: ORKTextChoiceAnswerFormat = ORKAnswerFormat.choiceAnswerFormat(with: .singleChoice, textChoices: question47Choices)
    // let question47Step = ORKQuestionStep(identifier: "47", title: nil, question: "How would you describe your cigarette smoking?", answer: question47)
    // question47Step.isOptional = true
    // steps += [question47Step]
    //
    // let question48: ORKHeightAnswerFormat = ORKAnswerFormat.heightAnswerFormat(with: .local)
    // let question48step = ORKQuestionStep(identifier: "48", title: nil, question: "What is your height?", answer: question48)
    // question48step.isOptional = true
    // steps += [question48step]
    //
    // let question49: ORKWeightAnswerFormat = ORKAnswerFormat.weightAnswerFormat(with: .local, numericPrecision: .default)
    // let question49step = ORKQuestionStep(identifier: "49", title: nil, question: "What is your weight?", answer: question49)
    // question49step.isOptional = true
    // steps += [question49step]
    
    let summaryStep = ORKCompletionStep(identifier: "SummaryStep")
    summaryStep.title = "Survey Completed!"
    summaryStep.text = "Thank you for participating in this study! Your contributions will help us find better ways of managing patients with back and leg pain."
    steps += [summaryStep]
    
    // method 1
    // let resultSelector: ORKResultSelector = ORKResultSelector(resultIdentifier: question44Step.identifier)
    // let askEmployment = NSCompoundPredicate(notPredicateWithSubpredicate: ORKResultPredicate.predicateForChoiceQuestionResult(with: resultSelector, matchingPattern: "Other"))
    // let employmentNavigationRule = ORKPredicateSkipStepNavigationRule(resultPredicate: askEmployment)
    
    // // method 2
    // let predicate44to46 = ORKResultPredicate.predicateForChoiceQuestionResult(with: resultSelector, matchingPattern: "Other")
    // let rule44to46 = ORKPredicateStepNavigationRule(resultPredicatesAndDestinationStepIdentifiers: [(predicate44to46, question46Step.identifier)])
    // print(rule44to46)
    
    let task = ORKNavigableOrderedTask(identifier: "Minimum Dataset", steps: steps)
    // task.setNavigationRule(rule10to12, forTriggerStepIdentifier: question10Step.identifier)
    // task.setSkip(employmentNavigationRule, forStepIdentifier: question45Step.identifier)
    task.shouldReportProgress = true
    
    // return ORKOrderedTask(identifier: "SurveyTask", steps: steps)
    return task
}
