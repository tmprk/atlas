//
//  PinView.swift
//  Atlas
//
//  Created by Tim Park on 9/29/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import Foundation
import UIKit

public enum OTPError: Error {
    
    case inCompleteOTPEntry
    
    public var localizedDescription: String {
        switch self {
        case .inCompleteOTPEntry:  return "Incomplete OTP Entry"
        }
    }
    
}

public enum OTPLength {
    
    case four
    case six
    case custom(Int)
    
    var value: Int {
        switch self {
        case .four: return 4
        case .six: return 6
        case .custom(let num): return num
        }
    }
    
    var lineLength: Int? {
        switch self {
        case .four: return 40
        case .six: return 30
        case .custom: return nil
        }
    }
    
}

protocol OTPFilledDelegate: class {
    func updateButton(isFilled: Bool)
}

class PinView: UIStackView, UITextFieldDelegate, OTPTextFieldDelegate {
    
    lazy public var config: PinConfig! = PinConfig()
    var textFields = [UITextField]()
    weak var delegate: OTPFilledDelegate?
    
    // MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    convenience init(config: PinConfig = PinConfig()) {
        self.init()
        self.config = config
        self.spacing = config.spacing!
        self.axis = .horizontal
        self.alignment = .fill
        self.distribution = .fillEqually
        // setUpView()
    }
    
    public func setUpView() {
        for _ in 0..<config.otpLength!.value {
            let view = UIView()
            view.backgroundColor = .clear
            addArrangedSubview(view)
        }
        
        self.layoutIfNeeded()
        
        for i in stride(from: 0, to: config.otpLength!.value, by: 1) {
            let view = self.arrangedSubviews[i]
            let frameSize: CGSize = self.arrangedSubviews[i].frame.size
            let textField: PinTextField = PinTextField()
            textField.translatesAutoresizingMaskIntoConstraints = false
            textField.delegateOTP = self
            textField.tag = i + 100
            textField.frame = CGRect(x: 0, y: 0, width: frameSize.width * 0.9, height: frameSize.height - 1)
            textField.delegate = self
            textField.isSecureTextEntry = config.isSecureTextEntry!
            textField.placeholder = config.showPlaceHolder! ? config.placeHolderText : ""
            
            // textField.backgroundColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0)
            textField.backgroundColor = .secondarySystemBackground
            textField.textColor = .label
            textField.layer.cornerRadius = 5
            
            textField.textAlignment = .center
            textField.borderStyle = .none
            textField.keyboardType = .numberPad
            // textField.textColor = config.dotColor
            // textField.font = UIFont.boldSystemFont(ofSize: 25.0)
            if UIDevice.current.userInterfaceIdiom == .pad {
                textField.font = UIFont.boldSystemFont(ofSize: 30.0)
            } else {
                textField.font = UIFont.boldSystemFont(ofSize: 25.0)
            }
            
            textField.tintColor = .clear
            view.addSubview(textField)
            
            let textFieldConstraints = [
                textField.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                textField.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                textField.topAnchor.constraint(equalTo: view.topAnchor),
                textField.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            ]
            
            NSLayoutConstraint.activate(textFieldConstraints)
            self.textFields.append(textField)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.count == 0 {
            if textField.text?.count == 0 {
                if let previousTextField = self.viewWithTag(textField.tag-1) {
                    previousTextField.becomeFirstResponder()
                }
            }
        } else {
            if let currentTextField = self.viewWithTag(textField.tag+1) {
                currentTextField.becomeFirstResponder()
            } else {
                if !textField.text!.isEmpty {
                    return false
                }
            }
        }
        textField.text = string
        self.delegate?.updateButton(isFilled: self.getCode() == 6)
        return false
    }
    
    func getCode() -> Int {
        var length = 0
        for textField in textFields {
            if textField.text != "" {
                length += 1
            }
        }
        return length
    }
    
    func getOTP() throws -> String {
        var otpCode: String = ""
        for textField in textFields {
            if textField.text == "" {
                throw OTPError.inCompleteOTPEntry
            }
            otpCode += textField.text!
        }
        return otpCode
    }
    
    func OTPTextFieldDidPressBackspace(textfield: PinTextField) {
        if let previousTextField: PinTextField = self.viewWithTag(textfield.tag - 1) as? PinTextField {
            previousTextField.text = ""
            previousTextField.becomeFirstResponder()
        }
    }
}
