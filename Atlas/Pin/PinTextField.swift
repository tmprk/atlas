//
//  PinTextField.swift
//  Atlas
//
//  Created by Tim Park on 9/29/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import Foundation
import UIKit

protocol OTPTextFieldDelegate {
    func OTPTextFieldDidPressBackspace(textfield: PinTextField)
}

class PinTextField: UITextField {

    var delegateOTP: OTPTextFieldDelegate!
    
    override func deleteBackward() {
        super.deleteBackward()
        
        if delegateOTP != nil {
            delegateOTP.OTPTextFieldDidPressBackspace(textfield: self)
        }
    }
    
}
