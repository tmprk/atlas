//
//  RootViewController.swift
//  Atlas
//
//  Created by Tim Park on 11/3/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit
import ResearchKit

struct NotifData {
    var task: ORKTask
    var identifier: String
}

class MainNavigationController: UINavigationController { }

class RootViewController: UIViewController, Alertable {

    private var current: UIViewController
    var notification: NotifData? {
        didSet {
            handleDeeplink()
        }
    }
    
    var deeplink: DeeplinkType? {
        didSet {
            handleDeeplink()
        }
    }
    
    init() {
        current = SplashViewController()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addChild(current)
        current.view.frame = view.bounds
        view.addSubview(current.view)
        current.didMove(toParent: self)
    }
    
    func showLoginScreen() {
        let new = UINavigationController(rootViewController: EntryController())
        
        addChild(new)
        new.view.frame = view.bounds
        view.addSubview(new.view)
        new.didMove(toParent: self)
        
        current.willMove(toParent: nil)
        current.view.removeFromSuperview()
        current.removeFromParent()
        
        current = new
    }
    
    func switchToLogout() {
        let loginViewController = EntryController()
        let logoutScreen = UINavigationController(rootViewController: loginViewController)
        animateDismissTransition(to: logoutScreen)
    }
    
    func switchToResearchDashboard() {
        let mainViewController = ResearcherDashboard()
        let mainScreen = MainNavigationController(rootViewController: mainViewController)
        animateFadeTransition(to: mainScreen) { [weak self] in
            print("researcher")
            self?.handleDeeplink()
        }
    }
    
    func switchToPatientDashboard() {
        let mainViewController = TabBarController()
        animateFadeTransition(to: mainViewController) {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if let notif = appDelegate.notificationData {
                let splitController = (mainViewController.mainViewController as! UISplitViewController)
                let patientDashboard = ((splitController.viewControllers[0] as! UINavigationController).viewControllers[0] as! PatientDashboard)
                patientDashboard.presentSurvey(task: notif.task)
            }
        }
    }
    
    private func animateFadeTransition(to new: UIViewController, completion: (() -> Void)? = nil) {
        current.willMove(toParent: nil)
        addChild(new)
        transition(from: current, to: new, duration: 0.3, options: [.transitionCrossDissolve, .curveEaseOut], animations: {
            
        }) { completed in
            self.current.removeFromParent()
            new.didMove(toParent: self)
            self.current = new
            completion?()
        }
    }
    
    private func animateDismissTransition(to new: UIViewController, completion: (() -> Void)? = nil) {
        let initialFrame = CGRect(x: -view.bounds.width, y: 0, width: view.bounds.width, height: view.bounds.height)
        current.willMove(toParent: nil)
        addChild(new)
        new.view.frame = initialFrame
        
        transition(from: current, to: new, duration: 0.3, options: [.curveEaseInOut], animations: {
            new.view.frame = self.view.bounds
        }) { completed in
            self.current.removeFromParent()
            new.didMove(toParent: self)
            self.current = new
            completion?()
        }
    }
    
    private func handleDeeplink() {
        if let mainNavigationController = current as? MainNavigationController, let deeplink = deeplink {
            mainNavigationController.popToRootViewController(animated: false)
            
            // switch deeplink {
            // case .activity:
            //     print("Show Activity Screen")
            //     mainNavigationController.popToRootViewController(animated: false)
            //     // (mainNavigationController.topViewController as? ResearcherDashboard)?.showActivityScreen()
            // default:
            //     // handle any other types of Deeplinks here
            //     break
            // }
            // // reset the deeplink back no nil, so it will not be triggered more than once
            // self.deeplink = nil
        }
    }
}
