//
//  ViewController.swift
//  Atlas
//
//  Created by Tim Park on 9/24/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

class EntryController: UIViewController {
    
    var screenWidth = UIScreen.main.bounds.width
    var screenHeight = UIScreen.main.bounds.height
    
    lazy var logoImage: UIImageView = {
        let logoImage = UIImageView(frame: CGRect(x: 0, y: 0, width: screenWidth * 0.35, height: screenWidth * 0.35))
        logoImage.translatesAutoresizingMaskIntoConstraints = false
        logoImage.layer.cornerRadius = logoImage.frame.size.width / 4
        logoImage.layer.masksToBounds = true
        return logoImage
    }()
    
    let titleText: UILabel = {
        let titleText = UILabel()
        titleText.translatesAutoresizingMaskIntoConstraints = false
        titleText.text = "Atlas \n Injection Tracker"
        // titleText.font = .systemFont(ofSize: 35, weight: .semibold)
        if UIDevice.current.userInterfaceIdiom == .pad {
            titleText.font = .systemFont(ofSize: 55, weight: .semibold)
        } else {
            titleText.font = .systemFont(ofSize: 35, weight: .semibold)
        }
        titleText.textAlignment = .center
        titleText.textColor = .label
        titleText.numberOfLines = 2
        return titleText
    }()
    
    let researcherButton: BouncyButton = {
        let button = BouncyButton(type: UIButton.ButtonType.custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.adjustsImageWhenHighlighted = false
        button.layer.cornerRadius = 12
        button.backgroundColor = .systemPurple
        button.setTitle("Researcher Login", for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.5
        button.setTitleColor(.white, for: .normal)
        // button.titleLabel?.font = .systemFont(ofSize: 22, weight: .medium)
        if UIDevice.current.userInterfaceIdiom == .pad {
            button.titleLabel?.font = .systemFont(ofSize: 30, weight: .bold)
        } else {
            button.titleLabel?.font = .systemFont(ofSize: 22, weight: .medium)
        }
        return button
    }()
    
    let patientButton: BouncyButton = {
        let button = BouncyButton(type: UIButton.ButtonType.custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.adjustsImageWhenHighlighted = false
        button.layer.cornerRadius = 12
        button.backgroundColor = .systemBlue
        button.setTitle("Patient Login", for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.5
        button.setTitleColor(.white, for: .normal)
        // button.titleLabel?.font = .systemFont(ofSize: 22, weight: .medium)
        if UIDevice.current.userInterfaceIdiom == .pad {
            button.titleLabel?.font = .systemFont(ofSize: 30, weight: .bold)
        } else {
            button.titleLabel?.font = .systemFont(ofSize: 22, weight: .medium)
        }
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .systemBackground
        self.navigationController?.navigationBar.barTintColor = .systemBackground
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        setupButtons()
        researcherButton.addTarget(self, action: #selector(setupResearcherLogin), for: .touchUpInside)
        patientButton.addTarget(self, action: #selector(setupPatientLogin), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.all)
    }
    
}

extension EntryController {
    
    @objc func setupResearcherLogin() {
        let modalVC = ResearcherLogin()
        self.navigationController?.pushViewController(modalVC, animated: true)
    }
    
    @objc func setupPatientLogin() {
        let modalVC = PatientLogin()
        self.navigationController?.pushViewController(modalVC, animated: true)
    }
    
    private func setupButtons() {
        view.addSubview(logoImage)
        view.addSubview(titleText)
        view.addSubview(researcherButton)
        view.addSubview(patientButton)
        
        var titleTextTopConstraint: NSLayoutConstraint!
        var logoImagetop: CGFloat!
        var patientButtonLeading, patientButtonBottom, patientButtonTrailing, patientButtonHeight: CGFloat!
        var researcherButtonLeading, researcherButtonTrailing, researcherButtonBottom, researcherButtonHeight: CGFloat!
        
        logoImage.image = UIImage(named: "logoImage")
        if UIDevice.current.userInterfaceIdiom == .pad {
            titleTextTopConstraint = titleText.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: screenHeight / 10)
            logoImagetop = -screenHeight / 15
            
            patientButtonLeading = screenHeight / 10
            patientButtonBottom = -screenHeight / 20
            patientButtonTrailing = -screenHeight / 10
            patientButtonHeight = screenHeight / 13
            
            researcherButtonLeading = screenHeight / 10
            researcherButtonTrailing = -screenHeight / 10
            researcherButtonBottom = -screenHeight / 45
            researcherButtonHeight = screenHeight / 13
        } else {
            logoImagetop = -screenHeight / 13
            titleTextTopConstraint = titleText.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: screenHeight / 10)
            
            patientButtonLeading = 15
            patientButtonBottom = -15
            patientButtonTrailing = -15
            patientButtonHeight = screenHeight / 9.5
            
            researcherButtonLeading = 15
            researcherButtonTrailing = -15
            researcherButtonBottom = -15
            researcherButtonHeight = screenHeight / 9.5
        }
        
        let buttonConstraints = [
            // logoImage.topAnchor.constraint(equalTo: self.titleText.bottomAnchor, constant: logoImagetop),
            logoImage.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.35),
            logoImage.heightAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.35),
            logoImage.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            logoImage.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: logoImagetop),
            
            titleText.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            titleText.topAnchor.constraint(equalTo: self.logoImage.bottomAnchor, constant: 15),
            
            patientButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: patientButtonLeading),
            patientButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: patientButtonBottom),
            patientButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: patientButtonTrailing),
            patientButton.heightAnchor.constraint(equalToConstant: patientButtonHeight),
            
            researcherButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: researcherButtonLeading),
            researcherButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: researcherButtonTrailing),
            researcherButton.bottomAnchor.constraint(equalTo: self.patientButton.topAnchor, constant: researcherButtonBottom),
            researcherButton.heightAnchor.constraint(equalToConstant: researcherButtonHeight)
        ]
        NSLayoutConstraint.activate(buttonConstraints)
        titleTextTopConstraint.isActive = true
    }
    
}
