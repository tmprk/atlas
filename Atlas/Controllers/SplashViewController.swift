//
//  SplashViewController.swift
//  Atlas
//
//  Created by Tim Park on 11/3/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit
import Firebase

class SplashViewController: UIViewController {
    
    private var db = Firestore.firestore()
    let defaults = UserDefaults.standard
    private let activityIndicator = UIActivityIndicatorView(style: .large)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        // change to icon color
        // view.backgroundColor = UIColor(red: 253/255, green: 156/255, blue: 153/255, alpha: 1.0)
        
        view.addSubview(activityIndicator)
        activityIndicator.frame = view.bounds
        activityIndicator.backgroundColor = .systemBackground
        makeServiceCall()
    }
    
    private func makeServiceCall() {
        activityIndicator.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
            self.activityIndicator.stopAnimating()
            // test to see flow
            if let uniqueID = self.defaults.string(forKey: "PATIENT_ID") {
                if let token = Messaging.messaging().fcmToken {
                    let usersRef = self.db.collection("patients").document(uniqueID)
                    usersRef.setData(["fcmToken": token], merge: true)
                }
                print("ok, patient id is stored on phone, but is it in the database?")
                let patientRef = self.db.collection("patients").document(uniqueID)
                patientRef.getDocument { [weak self] (document, error) in
                    if document != nil {
                        print("there is a PatientID, must be a patient who enrolled, and they are in the database")
                        (UIApplication.shared.windows.first?.rootViewController as! RootViewController).switchToPatientDashboard()
                    } else {
                        self?.removeDefaults()
                        print("Patient does not exist in the database")
                        (UIApplication.shared.windows.first?.rootViewController as! RootViewController).showLoginScreen()
                    }
                }
            } else {
                print("no PatientID, must be a researcher or new device")
                if self.defaults.bool(forKey: "LOGGED_IN") {
                    (UIApplication.shared.windows.first?.rootViewController as! RootViewController).switchToResearchDashboard()
                } else {
                    (UIApplication.shared.windows.first?.rootViewController as! RootViewController).showLoginScreen()
                }
            }
        }
    }
    
    func removeDefaults() {
        let domain = Bundle.main.bundleIdentifier!
        defaults.removePersistentDomain(forName: domain)
        defaults.synchronize()
    }
    
}
