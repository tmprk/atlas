//
//  ResearcherLogin.swift
//  Atlas
//
//  Created by Tim Park on 9/24/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit
import Firebase

class ResearcherLogin: UIViewController, UITextFieldDelegate {
    
    private var db = Firestore.firestore()
    var screenWidth = UIScreen.main.bounds.width
    var screenHeight = UIScreen.main.bounds.height
    var buttonConstraint: NSLayoutConstraint!
    
    let dialog: UILabel = {
        let dialog = UILabel()
        dialog.translatesAutoresizingMaskIntoConstraints = false
        dialog.textColor = UIColor.systemRed
        dialog.font = .systemFont(ofSize: 14)
        return dialog
    }()
    
    let emailBox: TextField = {
        let emailBox = TextField()
        emailBox.translatesAutoresizingMaskIntoConstraints = false
        emailBox.backgroundColor = .secondarySystemBackground
        emailBox.textColor = .label
        emailBox.textAlignment = .left
        emailBox.layer.cornerRadius = 12
        if UIDevice.current.userInterfaceIdiom == .pad {
            emailBox.attributedPlaceholder = NSAttributedString(string: "Enter Email", attributes: [.foregroundColor: UIColor(red:0.62, green:0.62, blue:0.62, alpha:1.0), .font: UIFont.systemFont(ofSize: 35, weight: .medium)])
            emailBox.font = UIFont.systemFont(ofSize: 32, weight: .medium)
        } else {
            emailBox.attributedPlaceholder = NSAttributedString(string: "Enter Email", attributes: [.foregroundColor: UIColor(red:0.62, green:0.62, blue:0.62, alpha:1.0), .font: UIFont.systemFont(ofSize: 20.5, weight: .medium)])
            emailBox.font = UIFont.systemFont(ofSize: 20.5, weight: .medium)
        }
        emailBox.textContentType = .emailAddress
        emailBox.clipsToBounds = false
        emailBox.autocapitalizationType = .none
        return emailBox
    }()
    
    let passwordBox: TextField = {
        let passwordBox = TextField()
        passwordBox.translatesAutoresizingMaskIntoConstraints = false
        passwordBox.isSecureTextEntry = true
        passwordBox.backgroundColor = .secondarySystemBackground
        passwordBox.textColor = .label
        passwordBox.textAlignment = .left
        passwordBox.layer.cornerRadius = 12
        if UIDevice.current.userInterfaceIdiom == .pad {
            passwordBox.attributedPlaceholder = NSAttributedString(string: "Enter Password", attributes: [.foregroundColor: UIColor(red:0.62, green:0.62, blue:0.62, alpha:1.0), .font: UIFont.systemFont(ofSize: 35, weight: .medium)])
            passwordBox.font = UIFont.systemFont(ofSize: 32, weight: .medium)
        } else {
            passwordBox.attributedPlaceholder = NSAttributedString(string: "Enter Password", attributes: [.foregroundColor: UIColor(red:0.62, green:0.62, blue:0.62, alpha:1.0), .font: UIFont.systemFont(ofSize: 20.5, weight: .medium)])
            passwordBox.font = UIFont.systemFont(ofSize: 20.5, weight: .medium)
        }
        passwordBox.clipsToBounds = false
        return passwordBox
    }()
    
    lazy var loginButton: BouncyButton = {
        let button = BouncyButton(type: UIButton.ButtonType.custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .systemBlue
        button.setTitle("Login / Register", for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.5
        button.setTitleColor(.white, for: .normal)
        button.adjustsImageWhenHighlighted = false
        button.layer.cornerRadius = 12
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.isEnabled = false
        button.alpha = 0.5
        if UIDevice.current.userInterfaceIdiom == .pad {
            button.titleLabel?.font = .systemFont(ofSize: 30, weight: .bold)
        } else {
            button.titleLabel?.font = .systemFont(ofSize: 22, weight: .medium)
        }
        return button
    }()
    
}

extension ResearcherLogin {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        layoutViews()
        setupKeyboardAndButtonConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        emailBox.becomeFirstResponder()
    }
    
    func setupKeyboardAndButtonConstraints() {
        buttonConstraint = loginButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -screenHeight / 35)
        buttonConstraint.isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: screenHeight / 12).isActive = true
        self.view.layoutIfNeeded()
        subscribeToShowKeyboardNotifications()
        [emailBox, passwordBox].forEach({ $0.addTarget(self, action: #selector(editingChanged), for: .editingChanged) })
    }
    
    @objc func setupNavigation() {
        self.title = "Researcher Login"
        self.view.backgroundColor = .systemBackground
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.label,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .medium)
        ]
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = .systemBackground
        self.navigationController?.navigationBar.tintColor = .label
    }
    
    func layoutViews() {
        self.view.addSubview(dialog)
        self.view.addSubview(emailBox)
        self.view.addSubview(passwordBox)
        self.view.addSubview(loginButton)
        
        var emailBoxLeading, emailBoxTop, emailBoxTrailing, emailBoxHeight: CGFloat!
        var passwordBoxLeading, passwordBoxTop, passwordBoxTrailing, passwordBoxHeight: CGFloat!
        var dialogLeading, dialogTop, dialogTrailing: CGFloat!
        var loginButtonLeading, loginButtonTrailing: CGFloat!
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            emailBoxLeading = screenHeight / 10
            emailBoxTop = screenHeight / 20
            emailBoxTrailing = -screenHeight / 10
            emailBoxHeight = screenHeight / 12
            
            passwordBoxLeading = screenHeight / 10
            passwordBoxTop = screenHeight / 40
            passwordBoxTrailing = -screenHeight / 10
            passwordBoxHeight = screenHeight / 12
            
            dialogLeading = 20
            dialogTop = 10
            dialogTrailing = -20
                
            loginButtonLeading = screenHeight / 10
            loginButtonTrailing = -screenHeight / 10
        } else {
            emailBoxLeading = 20
            emailBoxTop = 20
            emailBoxTrailing = -20
            emailBoxHeight = screenHeight / 12
            
            passwordBoxLeading = 20
            passwordBoxTop = 15
            passwordBoxTrailing = -20
            passwordBoxHeight = screenHeight / 12
            
            dialogLeading = 20
            dialogTop = 10
            dialogTrailing = -20
                
            loginButtonLeading = 20
            loginButtonTrailing = -20
        }
        
        let formConstraints = [
            emailBox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: emailBoxLeading),
            emailBox.topAnchor.constraint(equalTo: self.view.topAnchor, constant: emailBoxTop),
            emailBox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: emailBoxTrailing),
            emailBox.heightAnchor.constraint(equalToConstant: emailBoxHeight),
            
            passwordBox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: passwordBoxLeading),
            passwordBox.topAnchor.constraint(equalTo: self.emailBox.bottomAnchor, constant: passwordBoxTop),
            passwordBox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: passwordBoxTrailing),
            passwordBox.heightAnchor.constraint(equalToConstant: passwordBoxHeight),
            
            dialog.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: dialogLeading),
            dialog.topAnchor.constraint(equalTo: self.passwordBox.bottomAnchor, constant: dialogTop),
            dialog.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: dialogTrailing),
            
            loginButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: loginButtonLeading),
            loginButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: loginButtonTrailing)
        ]
        NSLayoutConstraint.activate(formConstraints)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        buttonConstraint.constant = (-screenHeight / 35) - keyboardHeight
        
        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        buttonConstraint.constant = (-screenHeight / 35)
        
        let userInfo = notification.userInfo
        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func buttonAction() {
        tryCreateUser()
    }
    
    func hideKeyboard() {
        view.endEditing(true)
    }
    
    @objc func tryCreateUser() {
        guard let email = emailBox.text, let password = passwordBox.text else { return }
        loginButton.showLoading()
        Auth.auth().fetchSignInMethods(forEmail: email) { (providers, error) in
            if providers != nil {
                // attempt to sign in user
                Auth.auth().signIn(withEmail: email, password: password) { [weak self] authResult, error in
                    guard let strongSelf = self else { return }
                    if let error = error {
                        strongSelf.dialog.text = "Invalid Credentials"
                        strongSelf.dialog.fadeIn()
                        strongSelf.loginButton.hideLoading()
                        print(error.localizedDescription)
                        return
                    }
                    strongSelf.loginButton.hideLoading()
                    UserDefaults.standard.set(true, forKey: "LOGGED_IN")
                    (UIApplication.shared.windows.first?.rootViewController as! RootViewController).switchToResearchDashboard()
                }
            } else {
                // attempt to create user
                Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                    guard let user = authResult?.user, error == nil else {
                        print(error!.localizedDescription)
                        return
                    }
                    print("\(user.email!) created!")
                    self.dialog.fadeOut()
                    self.loginButton.hideLoading()
                    self.hideKeyboard()
                    let fcm = Messaging.messaging().fcmToken!
                    UserDefaults.standard.set(true, forKey: "LOGGED_IN")
                    self.db.collection("researchers").document(email).setData(["fcmToken" : fcm]) { (error) in
                        (UIApplication.shared.windows.first?.rootViewController as! RootViewController).switchToResearchDashboard()
                    }
                }
            }
        }
        
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    @objc func close() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func editingChanged(_ textField: UITextField) {
        if textField.text?.count == 1 {
            if textField.text?.first == " " {
                textField.text = ""
                return
            }
        }
        guard
            let emailText = emailBox.text, !emailText.isEmpty && isValidEmail(emailStr: emailText),
            let passwordText = passwordBox.text, passwordText.count >= 6
            else {
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.loginButton.isEnabled = false
                    self.loginButton.alpha = 0.5
                }, completion: nil)
                return
        }
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            self.loginButton.isEnabled = true
            self.loginButton.alpha = 1.0
        }, completion: nil)
    }
    
}
