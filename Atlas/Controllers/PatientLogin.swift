//
//  PatientLogin.swift
//  Atlas
//
//  Created by Tim Park on 9/24/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class PatientLogin: UIViewController, UITextFieldDelegate, OTPFilledDelegate {
    
    let defaults = UserDefaults.standard
    var screenWidth = UIScreen.main.bounds.width
    var screenHeight = UIScreen.main.bounds.height
    var buttonConstraint: NSLayoutConstraint!
    var db: Firestore!
    
    lazy var loginButton: BouncyButton = {
        let button = BouncyButton(type: UIButton.ButtonType.custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .systemBlue
        button.setTitle("Login", for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.titleLabel?.minimumScaleFactor = 0.5
        button.setTitleColor(.white, for: .normal)
        button.adjustsImageWhenHighlighted = false
        button.layer.cornerRadius = 12
        button.translatesAutoresizingMaskIntoConstraints = false
        // button.titleLabel?.font = .systemFont(ofSize: 22, weight: .medium)
        if UIDevice.current.userInterfaceIdiom == .pad {
            button.titleLabel?.font = .systemFont(ofSize: 30, weight: .bold)
        } else {
            button.titleLabel?.font = .systemFont(ofSize: 22, weight: .medium)
        }
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button.isEnabled = false
        button.alpha = 0.5
        return button
    }()
    
    let dialog: UILabel = {
        let dialog = UILabel()
        dialog.translatesAutoresizingMaskIntoConstraints = false
        dialog.text = "Enter the 6-digit pin number given by your research coordinator"
        dialog.textColor = .label
        dialog.numberOfLines = 0
        if UIDevice.current.userInterfaceIdiom == .pad {
            dialog.font = .systemFont(ofSize: 20, weight: .semibold)
        } else {
            dialog.font = .systemFont(ofSize: 14)
        }
        return dialog
    }()
    
    lazy var pinView: PinView = {
        let pinView = PinView()
        pinView.translatesAutoresizingMaskIntoConstraints = false
        pinView.distribution = .fillEqually
        pinView.axis = .horizontal
        pinView.alignment = .fill
        pinView.backgroundColor = .clear
        // pinView.spacing = 10
        if UIDevice.current.userInterfaceIdiom == .pad {
            pinView.spacing = 28
        } else {
            pinView.spacing = 10
        }
        pinView.delegate = self
        return pinView
    }()
    
    let noticeLabel: UILabel = {
        let noticeLabel = UILabel()
        noticeLabel.translatesAutoresizingMaskIntoConstraints = false
        noticeLabel.textColor = UIColor.systemRed
        if UIDevice.current.userInterfaceIdiom == .pad {
            noticeLabel.font = .systemFont(ofSize: 20, weight: .semibold)
        } else {
            noticeLabel.font = .systemFont(ofSize: 14)
        }
        noticeLabel.text = ""
        return noticeLabel
    }()
    
}

extension PatientLogin {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDatabase()
        setupNavigation()
        layoutViews()
        setupKeyboardAndButtonConstraints()
        subscribeToShowKeyboardNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pinView.textFields[0].becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.all)
    }
    
    func setupDatabase() {
        db = Firestore.firestore()
    }
    
    func setupNavigation() {
        self.title = "Patient Login"
        self.view.backgroundColor = .systemBackground
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.label,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .medium)
        ]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = .systemBackground
        self.navigationController?.navigationBar.tintColor = .label
    }
    
    func setupKeyboardAndButtonConstraints() {
        buttonConstraint = loginButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -screenHeight / 35)
        buttonConstraint.isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: screenHeight / 12).isActive = true
        self.view.layoutIfNeeded()
    }
    
    func setupPinView() {
        var config: PinConfig! = PinConfig()
        config.otpLength = .six
        config.dotColor = .black
        config.lineColor = #colorLiteral(red: 0.8265652657, green: 0.8502194881, blue: 0.9000532627, alpha: 1)
        // config.spacing = 20
        if UIDevice.current.userInterfaceIdiom == .pad {
            config.spacing = self.view.frame.size.width / 40
        } else {
            config.spacing = 20
        }
        config.isSecureTextEntry = false
        config.showPlaceHolder = false
        pinView.config = config
        pinView.setUpView()
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        buttonConstraint.constant = (-screenHeight / 35) - keyboardHeight
        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        buttonConstraint.constant = -screenHeight / 35
        let userInfo = notification.userInfo
        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func buttonAction() {
        let code = try? pinView.getOTP()
        loginButton.showLoading()
        db.collection("patients").getDocuments() { [weak self] (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self?.view.endEditing(true)
                for document in querySnapshot!.documents {
                    if code == document.documentID {
                        print("Success")
                        self?.loginButton.hideLoading()
                        self?.defaults.set(code, forKey: "PATIENT_ID")
                        self?.defaults.set(false, forKey: "LOGGED_IN")
                        if self?.defaults.string(forKey: "PATIENT_ID") != nil {
                            (UIApplication.shared.windows.first?.rootViewController as! RootViewController).switchToPatientDashboard()
                        }
                        break
                    } else {
                        print("patient not found")
                        self?.noticeLabel.fadeIn()
                        self?.noticeLabel.text = "Patient not found"
                    }
                }
                self?.loginButton.hideLoading()
            }
        }
    }
    
    @objc func close() {
        print("lol")
    }
    
    func layoutViews() {
        self.view.addSubview(dialog)
        self.view.addSubview(pinView)
        self.view.addSubview(noticeLabel)
        self.view.addSubview(loginButton)
        setupPinView()
        
        let dialogLeading, dialogTop, dialogTrailing: CGFloat!
        let pinViewLeading, pinViewTrailing, pinViewHeight: CGFloat!
        let loginButtonLeading, loginButtonTrailing: CGFloat!
        
        let insets = UIEdgeInsets(top: 20, left: 20, bottom: -20, right: -20)
        if UIDevice.current.userInterfaceIdiom == .pad {
            dialogLeading = screenHeight / 10
            dialogTop = screenHeight / 20
            dialogTrailing = -screenHeight / 10
            
            pinViewLeading = screenHeight / 10
            pinViewTrailing = -screenHeight / 10
            pinViewHeight = screenHeight / 12
            
            loginButtonLeading = screenHeight / 10
            loginButtonTrailing = -screenHeight / 10
        } else {
            dialogLeading = insets.left
            dialogTop = insets.top
            dialogTrailing = insets.right
            
            pinViewLeading = insets.left
            pinViewTrailing = -insets.left
            pinViewHeight = screenHeight / 13
            
            loginButtonLeading = insets.left
            loginButtonTrailing = insets.right
        }
        
        let formConstraints = [
            dialog.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: dialogLeading),
            dialog.topAnchor.constraint(equalTo: self.view.topAnchor, constant: dialogTop),
            dialog.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: dialogTrailing),
            
            pinView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: pinViewLeading),
            pinView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: pinViewTrailing),
            pinView.topAnchor.constraint(equalTo: self.dialog.bottomAnchor, constant: insets.top),
            pinView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            pinView.heightAnchor.constraint(equalToConstant: pinViewHeight),
            
            noticeLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: dialogLeading),
            noticeLabel.topAnchor.constraint(equalTo: self.pinView.bottomAnchor, constant: insets.top),
            noticeLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -insets.right),
            
            loginButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: loginButtonLeading),
            loginButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: loginButtonTrailing),
            loginButton.heightAnchor.constraint(equalToConstant: screenHeight / 12)
        ]
        NSLayoutConstraint.activate(formConstraints)
    }
    
    func updateButton(isFilled: Bool) {
        if isFilled {
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                self.loginButton.isEnabled = true
                self.loginButton.alpha = 1.0
            }, completion: nil)
            return
        } else {
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                self.loginButton.isEnabled = false
                self.loginButton.alpha = 0.5
            }, completion: nil)
            return
        }
    }
    
}

