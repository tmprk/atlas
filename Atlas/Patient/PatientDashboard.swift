//
//  PatientDashboard.swift
//  Atlas
//
//  Created by Tim Park on 10/2/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import ResearchKit
import UserNotifications

class PatientDashboard: UIViewController, Alertable {
    
    // let notificationDelegate = NotificationDelegate()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let defaults = UserDefaults.standard
    var db = Firestore.firestore()
    var patient: String!
    private var tableView: UITableView!
    private var headerView: ParallaxHeaderView!
    var localEvents = [Events]()
    // var surveyIndex: Int = 0
    weak var timer: Timer?
    var currentSurveyIdentifier: String!
    
    let progressBar: ProgressBar = {
        let progressBar = ProgressBar()
        progressBar.translatesAutoresizingMaskIntoConstraints = false
        progressBar.strokeColor = UIColor.systemBlue
        return progressBar
    }()
    
    let formatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        return formatter
    }()
    
    private func enableOffline() {
        let settings = db.settings
        settings.isPersistenceEnabled = true
        db.settings = settings
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enableOffline()
        
        // if (defaults.string(forKey: "SURVEY_INDEX") != nil) {
        //     surveyIndex = GlobalSettings.surveyIndex
        // }
        
        // general stuff
        setupNotfications()
        checkPatientId()
        generalSetup()
        setupTableView()
        
        // get updated documents and cross reference with core data local status
        checkProcedureAndResearcher()
        fetchScheduleLocally()
        setupHeaderMessageAndProgress()
        updateSurveyIndex()
        
        /* 1/3/2020
         startTimer()
        */
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("view will appear")
        self.navigationController?.setToolbarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        // startTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopTimer(nil)
    }
    
    func startTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            guard let strongSelf = self else { return }
            strongSelf.setupHeaderMessageAndProgress()
            let overdue = strongSelf.localEvents.filter({ (($0.rescheduled! < Date()) && ($0.completed == false) && ($0.skipped != true)) })
            if overdue.count >= 1 {
                strongSelf.stopTimer {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.65) {
                        strongSelf.presentLatestSchedule()
                    }
                }
            } else {
                let completed = strongSelf.localEvents.filter { $0.completed == true }
                let skipped = strongSelf.localEvents.filter { $0.skipped == true }
                let numberOfTrue = completed.count + skipped.count
                if numberOfTrue == strongSelf.localEvents.count {
                    print("hello")
                    strongSelf.stopTimer {
                        print("local schedule all completed")
                    }
                }
            }
        }
    }
    
    func stopTimer(_ completion: (() -> Void)?) {
        timer?.invalidate()
        timer = nil
        completion?()
    }
    
    deinit {
        stopTimer(nil)
    }
    
    func presentLatestSchedule() {
        var latestEvent: Events?
        if Procedure(id: defaults.string(forKey: "PROCEDURE")!) == .MBB {
            latestEvent = localEvents.last(where: { ($0.rescheduled! < Date()) && ($0.completed == false) && ($0.skipped != true) })
            if let latest = latestEvent {
                print("from index:", GlobalSettings.surveyIndex)
                GlobalSettings.surveyIndex = self.localEvents.firstIndex(of: latest)!
                // print("SURVEY INDEX IS", GlobalSettings.surveyIndex)
                print("to index:", GlobalSettings.surveyIndex)
            }
        } else {
            latestEvent = localEvents.first(where: {$0.rescheduled! < Date() && $0.completed == false})
        }
        if let latest = latestEvent {
            if GlobalSettings.surveyIndex == localEvents.firstIndex(of: latest) {
                let surveyTitle = latest.name!
                showAlert(title: "Confirm", message: "You are about to take the \(surveyTitle)") { [self] (success) in
                    if success == true {
                        GlobalSettings.surveyIndex = Int(latest.index)
                        // self.defaults.set(self.surveyIndex, forKey: "SURVEY_INDEX")
                        let surveyType = SurveyType(id: latest.surveyId!)
                        var task: ORKTask!
                        if surveyType == .Follow_Up {
                            var timeDifference: TimeInterval!
                            if (GlobalSettings.surveyIndex > 0) {
                                let lastSurveyDate = self.localEvents[GlobalSettings.surveyIndex - 1].date
                                let delta = Date().timeIntervalSince(lastSurveyDate!)
                                timeDifference = delta
                            }
                            let str = format(duration: timeDifference)
                            task = surveyType?.surveyId(title: surveyTitle, timeFrame: str)
                        } else {
                            task = surveyType?.surveyId()
                        }
                        self.presentSurvey(task: task)
                    }
                }
            } else {
                print("YOU HAVE MORE THAN ONE OVERDUE SURVEY")
            }
        } else {
            stopTimer(nil)
        }
    }
    
    func format(duration: TimeInterval) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.day, .hour, .minute, .second]
        formatter.unitsStyle = .full
        formatter.maximumUnitCount = 1
        formatter.includesApproximationPhrase = false
        formatter.zeroFormattingBehavior = .dropAll
        formatter.allowsFractionalUnits = false
        return formatter.string(from: duration)!
    }
    
    func setupHeaderMessageAndProgress() {
        if !localEvents.isEmpty {
            let completed = localEvents.filter { $0.completed == true }.count
            let skipped = localEvents.filter { $0.skipped == true }.count
            print(skipped)
            let totalCount = localEvents.count
            
            let done = CGFloat(completed + skipped)
            let percentage = done / CGFloat(totalCount)
            
            let date = localEvents.map { $0.rescheduled }
            let skippedString: String = (skipped > 0) ? "\(skipped) skipped." : "None skipped."
            if let upcomingDate = date.first(where: { $0! > Date() }) {
                let timeDiff = upcomingDate! - Date()
                headerView.message = "You have completed \(completed)/\(totalCount) surveys. " + skippedString + " Next survey in " + formatter.string(from: timeDiff)!
            } else {
                headerView.message = "\(completed)/\(totalCount) surveys completed, " + skippedString + " No more surveys remaining!"
                stopTimer(nil)
            }
            progressBar.progress = percentage
        }
    }
    
    func checkProcedureAndResearcher() {
        if ((defaults.string(forKey: "PROCEDURE") != nil) || !localEvents.isEmpty) {
            print("Procedure and schedule exists")
        } else {
            let docRef = db.collection("patients").document(patient!)
            docRef.getDocument { [weak self] (document, error) in
                if let document = document, document.exists {
                    if let dataDescription = document.data() {
                        let procedureString = dataDescription["type"] as! String
                        let researcherEmail = dataDescription["researcher"] as! String
                        print(procedureString)
                        self?.defaults.set(procedureString, forKey: "PROCEDURE")
                        self?.defaults.set(researcherEmail, forKey: "RESEARCHER")
                        
                        let procedure = Procedure.init(id: procedureString)
                        let schedule = NewSchedule(procedure: procedure!, startDate: Date())
                        let newScheduleKeys = schedule.agenda.keys
                        
                        let flattenedDictionary = newScheduleKeys
                            .flatMap { $0 }
                            .reduce([Date:String]()) { (dict, tuple) in
                                var nextDict = dict
                                nextDict.updateValue(tuple.1, forKey: tuple.0)
                                return nextDict
                        }
                        
                        let sortedSchedule = flattenedDictionary.sorted { lhs,rhs in
                            let l = (lhs.key).toLocalTime()
                            let r = (rhs.key).toLocalTime()
                            return l < r
                        }
                        print("Sorted Schedule", sortedSchedule)
                        
                        for (index, event) in sortedSchedule.enumerated() {
                            print("Index", index)
                            
                            let nested = sortedSchedule[index]
                            // let tmpDict: Dictionary<Date, String> = [nested.key:nested.value]
                            let tmpDict = Dictionary.init(elements: [nested])
                            print("nested", nested)
                            
                            let nestedValue = schedule.getValue(key: tmpDict)
                            print("name", nestedValue)
                            
                            let tableCellLabel = "\(nestedValue)" + " " + "\(event.value)"
                            print(tableCellLabel)
                            
                            self?.save(name: tableCellLabel, surveyId: event.value, date: event.key, completed: false, skipped: false, index: index, painScore: 0)
                            
                            if index != 0 {
                                print("event key:", event.key)
                                self?.appDelegate.notifications.append(Notif(title: "New Survey Available", body: "It is time to take the " + tableCellLabel, id: tableCellLabel, date: event.key, delivered: false))
                            }
                            print("")
                        }
                        self?.appDelegate.scheduleNotifications() // 1/3/2021

                        self?.fetchScheduleLocally()
                        self?.defaults.set(true, forKey: "SCHEDULE_CREATED")
                        self?.presentLatestSchedule()
                        print("Survey Index After Setup: " + (GlobalSettings.surveyIndex.description))
                    }
                }
            }
        }
    }
    
    @objc func fetchScheduleLocally() {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Events")
        let sort = NSSortDescriptor(key: #keyPath(Events.date), ascending: true)
        fetchRequest.sortDescriptors = [sort]
        do {
            let fetchedEvents = try managedContext.fetch(fetchRequest) as! [Events]
            localEvents = fetchedEvents
            print("this is how many (core data) events you are/were subscribed to \(fetchedEvents.count)")
            print("")
            for object in fetchedEvents {
                print("Core Data: Name: \(object.name!), Survey Index: \(object.index), Id: \(object.surveyId!), Date: \(object.date!), Completed: \(object.completed), painScore: \(object.painScore) skipped: \(object.skipped)")
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        print("")
        tableView.reloadData()
    }
    
    func setupNotfications() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.notificationCenter.delegate = appDelegate
        appDelegate.checkAuthorization { (result) in
            if result {
                print("You are authorized")
            } else {
                print("You are not authorized")
                self.showAlertWithSettings(title: "Push Notifications", message: "We noticed that your push notifications are off. Please turn them on in settings.")
            }
        }
        
        // 1/3/2021
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(_:)), name: Notification.Name.myNotificationKey, object: nil)
    }
    
    // 1/3/2021
    @objc func notificationReceived(_ notification: Notification) {
        // getting some data from surveyInfo is optional
        guard let text = notification.userInfo?["surveyType"] as? String else { return }
        print("This should work?", text)
        // presentLatestSchedule()
    }
    
    func checkPatientId() {
        if let patientID = defaults.string(forKey: "PATIENT_ID") {
            patient = patientID
            self.navigationItem.title = "Surveys"
        }
    }
    
    func save(name: String, surveyId: String, date: Date, completed: Bool, skipped: Bool, index: Int, painScore: Int) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Events", in: managedContext)!
        let event = NSManagedObject(entity: entity, insertInto: managedContext)
        event.setValue(name, forKey: "name")
        event.setValue(index, forKey: "index")
        event.setValue(date, forKey: "date")
        event.setValue(surveyId, forKeyPath: "surveyId")
        event.setValue(painScore, forKey: "painScore")
        event.setValue(date, forKey: "rescheduled")
        event.setValue(false, forKey: "completed")
        event.setValue(false, forKey: "skipped")
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func deleteAllData(_ entity: String) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                managedContext.delete(objectData)
            }
            try managedContext.save()
        } catch let error {
            print("Delete all data in \(entity) error :", error)
        }
    }
    
    func generalSetup() {
        edgesForExtendedLayout = []
        view.backgroundColor = .systemBackground
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.label,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .medium)
        ]
        navigationController?.navigationBar.barTintColor = .systemBackground
        navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.isToolbarHidden = false
        navigationController?.toolbar.isTranslucent = false
        // extendedLayoutIncludesOpaqueBars = false
        
        let items = [
            // UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(eraseCore)),
            // UIBarButtonItem(title: "x push", style: .plain, target: self, action: #selector(removeNotifications)),
            UIBarButtonItem(title: "sched", style: .plain, target: self, action: #selector(fetchScheduleLocally)),
            // UIBarButtonItem(title: "1", style: .plain, target: self, action: #selector(testButton3)),
            UIBarButtonItem(title: "notif", style: .plain, target: self, action: #selector(checkNotification))
        ]
        setToolbarItems(items, animated: false)
        
        // test for progress bar on toolbar
        if let toolbar = self.navigationController?.toolbar {
            toolbar.addSubview(progressBar)
            let progressBarInsets = UIEdgeInsets(top: 12, left: 12, bottom: -12, right: -12)
            let progressBarConstraints = [
                progressBar.leadingAnchor.constraint(equalTo: toolbar.leadingAnchor, constant: progressBarInsets.left),
                progressBar.topAnchor.constraint(equalTo: toolbar.topAnchor, constant: progressBarInsets.top),
                progressBar.trailingAnchor.constraint(equalTo: toolbar.trailingAnchor, constant: progressBarInsets.right),
                progressBar.bottomAnchor.constraint(equalTo: toolbar.bottomAnchor, constant: progressBarInsets.bottom)
            ]
            NSLayoutConstraint.activate(progressBarConstraints)
        }
        
        // let rightBarButtonItem = UIBarButtonItem(title: "logout", style: .plain, target: self, action: #selector(logout))
        // self.navigationItem.rightBarButtonItem = rightBarButtonItem
        // let leftBarButtonItem = UIBarButtonItem(title: "test", style: .plain, target: self, action: #selector(testButton3))
        // self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        let chatBarButtonItem = UIBarButtonItem(image: UIImage(named: "chat"), style: .plain, target: self, action: #selector(openChat))
        self.navigationItem.rightBarButtonItem = chatBarButtonItem
    }
    
    @objc func openChat() {
        let chatController = ChatController()
        chatController.hidesBottomBarWhenPushed = true
        if UIDevice.current.userInterfaceIdiom == .pad {
            print("iPad support")
            guard let splitViewController = splitViewController else { return }
            splitViewController.toggleMasterView()
            let detailNavigationController = (self.splitViewController!.viewControllers[1] as! UINavigationController)
            detailNavigationController.pushViewController(chatController, animated: true)
        } else {
            self.navigationController?.pushViewController(chatController, animated: true)
        }
    }
    
    // @objc func presentMinimum() {
    //     let taskViewController = ORKTaskViewController(task: MinimumDataset, taskRun: nil)
    //     taskViewController.delegate = self
    //     taskViewController.modalPresentationStyle = .pageSheet
    //     taskViewController.isModalInPresentation = true
    //     self.present(taskViewController, animated: true, completion: nil)
    // }
    
    @objc func removeNotifications() {
        appDelegate.notificationCenter.removeAllPendingNotificationRequests()
        print("removed all notifications")
    }
    
    // @objc func eraseCore() {
        // notificationDelegate.removePendingNotifications(ids: identifiersToRemove)
        // deleteAllData("Events")
        // print("erased all core data")
    // }
    
    @objc func testButton3() {
        print(GlobalSettings.surveyIndex)
        // self.notificationDelegate.schedule(Notif(title: "Test", body: "Test", id: "Test", date: Date().adding(seconds: 20), delivered: false))
        // print(String(describing: surveyIndex))
    }
    
    @objc func checkNotification() {
        print("check notifications")
        appDelegate.notificationCenter.getPendingNotificationRequests { (requests) in
            var nextTriggerDates: [Date] = []
            for request in requests {
                print("title of notification: \(request.content.title), identifier: \(request.identifier)")
                if let trigger = request.trigger as? UNCalendarNotificationTrigger,
                    let triggerDate = trigger.nextTriggerDate() {
                    nextTriggerDates.append(triggerDate)
                }
            }
            // for date in nextTriggerDates {
            //     print(date)
            // }
            
            // if let nextTriggerDate = nextTriggerDates.min() {
            //     print("next trigger date: \(nextTriggerDate)")
            //     print("")
            // }
        }
    }
    
    func setupTableView() {
        tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(EventCell.self, forCellReuseIdentifier: EventCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.showsVerticalScrollIndicator = false
        tableView.decelerationRate = .fast
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInsetAdjustmentBehavior = .never
        let parallaxViewFrame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height / 3)
        headerView = ParallaxHeaderView(frame: parallaxViewFrame)
        self.tableView.tableHeaderView = headerView
        tableView.tableFooterView = UIView()
        tableView.sectionFooterHeight = 0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600.0
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.alwaysBounceVertical = true
        tableView.bounces = true
        tableView.separatorStyle = .singleLine
        self.view.addSubview(tableView)
        let tableViewConstraints = [
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ]
        NSLayoutConstraint.activate(tableViewConstraints)
    }
    
    @objc func presentSurvey(task: ORKTask, identifier: String? = nil) {
        if identifier != nil {
            currentSurveyIdentifier = identifier
        }
        let taskViewController = ORKTaskViewController(task: task, taskRun: nil)
        taskViewController.delegate = self
        taskViewController.modalPresentationStyle = .pageSheet
        taskViewController.isModalInPresentation = true
        self.present(taskViewController, animated: true, completion: nil)
    }
    
    @objc func logout() {
        stopTimer { [weak self] in
            self?.removeNotifications()
            self?.deleteAllData("Events")
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            print("cleared all defaults and data")
            (UIApplication.shared.windows.first?.rootViewController as! RootViewController).switchToLogout()
        }
    }
    
}

extension PatientDashboard: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EventCell.identifier, for: indexPath) as! EventCell
        cell.event = localEvents[indexPath.row]
        cell.layer.masksToBounds = true
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if !localEvents.isEmpty {
            numOfSections = 1
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        } else {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "No Surveys Available"
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localEvents.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = .zero
        }
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.responds(to: #selector(setter: UITableViewCell.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        // Explictly set your cell's layout margins
        if cell.responds(to: #selector(setter: UITableViewCell.layoutMargins)) {
            cell.layoutMargins = .zero
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var fetchedResponses: [[String : Any]] = []
        let localSurvey = localEvents[indexPath.row]
        let localSurveyTitle = localSurvey.name!
        
        // print(localSurveyId)
        let documentRef = db.collection("patients").document(patient).collection("surveys").document(localSurveyTitle)
        documentRef.getDocument(source: .default) { (documentSnapshot, error) in
            if let fetchedDocument = documentSnapshot {
                print(fetchedDocument.exists)
                if fetchedDocument.exists {
                    let docId = fetchedDocument.documentID
                    print("1: \(docId)")
                    print("2: \(localSurveyTitle)")
                    if docId == localSurveyTitle {
                        print("this should print if the document exists in the database")
                        guard let documentData = fetchedDocument.data() else { return }
                        // let surveyCompletionDate = documentData["completionDate"] as! Timestamp
                        let responses = documentData["responses"] as! NSMutableArray
                        for response in responses {
                            let singleResponse = response as! [String : Any]
                            fetchedResponses.append(singleResponse)
                        }
                    }
                    let source = fetchedDocument.metadata.isFromCache ? "local cache" : "server"
                    print("Metadata: Data fetched from \(source)")
                }
                let surveyData = SurveyData(title: localSurveyTitle, responses: fetchedResponses)
                let responsesController = ResponsesController()
                responsesController.modalPresentationStyle = .formSheet
                responsesController.dataToPass = surveyData
                if UIDevice.current.userInterfaceIdiom == .pad {
                    guard let splitViewController = self.splitViewController else { return }
                    let existingResponsesController = (splitViewController.viewControllers[1] as! UINavigationController).viewControllers.first
                    guard let detailedResponsesController = existingResponsesController as? ResponsesController else { return }
                    detailedResponsesController.dataToPass = surveyData
                    detailedResponsesController.reloadTableView()
                } else {
                    self.present(UINavigationController(rootViewController: responsesController), animated: true, completion: nil)
                }
            }
            if let selectedRows = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: selectedRows, animated: true)
            }
        }
    }
    
}

extension PatientDashboard: ORKTaskViewControllerDelegate {
    
    func updateSurveyIndex() {
        for (index, event) in localEvents.enumerated() {
            if event.completed == false {
                GlobalSettings.surveyIndex = index
                print("next survey to take is \(String(describing: GlobalSettings.surveyIndex))")
                return
            }
        }
    }
    
    func taskViewController(_ taskViewController: ORKTaskViewController, didFinishWith reason: ORKTaskViewControllerFinishReason, error: Error?) {
        guard let task = taskViewController.task else { return }
        var responses: [Response] = []
        switch (reason) {
        case .completed:
            print("Completed")
            for stepResults in taskViewController.result.results! as! [ORKStepResult]  {
                for result in stepResults.results! {
                    let identifier = result.identifier
                    let questionStep = task.step?(withIdentifier: identifier) as! ORKQuestionStep
                    if let questionResult = result as? ORKScaleQuestionResult, let answerString = questionResult.scaleAnswer {
                        let questionString = questionStep.question!
                        print(identifier)
                        print("Question: " + questionString)
                        print("Answer: " + answerString.stringValue)
                        responses.append(Response(question: questionString, answer: answerString.stringValue))
                        print("")
                    } else if let questionResult = result as? ORKChoiceQuestionResult, let answerString = questionResult.choiceAnswers?.first as? String {
                        let questionString = questionStep.question!
                        print(identifier)
                        print("Question: " + questionString)
                        print("Answer: " + answerString)
                        responses.append(Response(question: questionString, answer: answerString))
                        print("")
                    } else if let questionResult = result as? ORKTextQuestionResult, let answerString = questionResult.textAnswer {
                        let questionString = questionStep.question!
                        print(identifier)
                        print("Question: " + questionString)
                        print("Answer: " + answerString)
                        responses.append(Response(question: questionString, answer: answerString))
                        print("")
                    } else if let questionResult = result as? ORKQuestionResult, let answerResult = questionResult.answer {
                        let questionString = questionStep.question!
                        print(identifier)
                        let numberAnswer = (answerResult as! NSNumber)
                        _ = String(describing: answerResult as! NSNumber)
                        print("Question: " + questionString)
                        if questionString.contains("height") {
                            let heightFt = showFootAndInchesFromCm(numberAnswer.doubleValue)
                            responses.append(Response(question: questionString, answer: heightFt))
                            print("Answer: " + heightFt)
                            print("")
                        } else if questionString.contains("weight") {
                            let weightKg = Measurement(value: numberAnswer.doubleValue, unit: UnitMass.kilograms)
                            let weightLb = weightKg.converted(to: .pounds)
                            
                            let n = NumberFormatter()
                            n.maximumFractionDigits = 2
                            let m = MeasurementFormatter()
                            m.numberFormatter = n
                            let weightLbStr = m.string(from: weightLb)
                            responses.append(Response(question: questionString, answer: weightLbStr))
                            
                            print("Answer: " + weightLbStr)
                            print("")
                        } else if questionString.contains("age") {
                            let age = String(numberAnswer.intValue)
                            responses.append(Response(question: questionString, answer: age))
                            print("Answer: " + age)
                            print("")
                        }
                    }
                }
            }
            
            let surveyDocumentTitle = localEvents[GlobalSettings.surveyIndex].name!
            let ref = db.collection("patients").document(patient).collection("surveys").document(surveyDocumentTitle)
            
            var painScore: Int!
            let responsesDict = responses.map { $0.dictionary }
            for i in responsesDict where (i["question"] as! String).contains("how would you rate your joint pain on average?") {
                painScore = Int(i["answer"] as! String)
            }
            
            let data: [String : Any] = [
                "completionDate": Timestamp(date: Date()),
                "responses" : responsesDict
            ]
            
            /* 1/3/2021
            ref.setData(data) { (error) in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    print("uploaded 🥳")
                    self.notificationDelegate.scheduleNotifications()
                }
            }
            */
            
            self.appDelegate.scheduleNotifications()
            
            let managedContext = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Events")
            let sort = NSSortDescriptor(key: #keyPath(Events.date), ascending: true)
            fetchRequest.sortDescriptors = [sort]
            
            do {
                let fetchedEvents = try managedContext.fetch(fetchRequest) as! [Events]
                if fetchedEvents.count != 0 {
                    fetchedEvents[GlobalSettings.surveyIndex].setValue(true, forKey: "completed")
                    fetchedEvents[GlobalSettings.surveyIndex].setValue(Date(), forKey: "date")
                    fetchedEvents[GlobalSettings.surveyIndex].setValue(painScore, forKey: "painScore")
                }
            } catch {
                print("Fetch Failed: \(error)")
            }
            
            for item in localEvents.prefix(upTo: GlobalSettings.surveyIndex) where (item.completed == false) && (item.index > 0) {
                item.setValue(true, forKey: "skipped")
                print(item.index, "is skipped")
            }
            
            do {
                try managedContext.save()
            }
            catch {
                print("Saving Core Data Failed: \(error)")
            }
            
            var identifiersToRemove: [String] = []
            for index in 1..<2 {
                let identifier = localEvents[GlobalSettings.surveyIndex].name! + "-reminder-\(index)"
                identifiersToRemove.append(identifier)
            }
            
            fetchScheduleLocally()
            GlobalSettings.surveyIndex += 1
            UIApplication.shared.applicationIconBadgeNumber = 0
            taskViewController.dismiss(animated: true, completion: { [weak self] in
                self?.appDelegate.removePendingNotifications(ids: identifiersToRemove)
                // self?.startTimer()
            })
        case .saved:
            print("Saved")
        case .discarded:
            print("Discarded")
            taskViewController.dismiss(animated: true) {
                if !self.appDelegate.notifications.isEmpty {
                    self.appDelegate.scheduleNotifications()
                }
            }
        case .failed:
            print("Failed")
        @unknown default:
            print("Unknown")
        }
    }
    
    func showFootAndInchesFromCm(_ cms: Double) -> String {
        let feet = cms * 0.0328084
        let feetShow = Int(floor(feet))
        let feetRest: Double = ((feet * 100).truncatingRemainder(dividingBy: 100) / 100)
        let inches = Int(floor(feetRest * 12))
        return "\(feetShow)' \(inches)\""
    }
    
}

extension PatientDashboard: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let headerView = self.tableView.tableHeaderView as! ParallaxHeaderView
        headerView.scrollViewDidScroll(scrollView)
    }
    
}
