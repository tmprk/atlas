//
//  ProgressController.swift
//  Atlas
//
//  Created by Tim Park on 11/16/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit
import CoreData

class ProgressController: UIViewController, LineChartDelegate, Alertable {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let screenHeight = UIScreen.main.bounds.height
    
    var portraitAnchor: NSLayoutConstraint!
    var landscapeAnchor: NSLayoutConstraint!
    var localEvents = [Events]()
    
    lazy var chart: Chart = {
        let chart = Chart(frame: .zero)
        chart.translatesAutoresizingMaskIntoConstraints = false
        chart.yLabels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        chart.showXLabelsAndGrid = true
        chart.showYLabelsAndGrid = true
        chart.highlightLineWidth = 4
        chart.lineWidth = 3.5
        chart.topInset = 0
        chart.xLabelsSkipLast = false
        chart.xLabelsOrientation = .vertical
        chart.hideHighlightLineOnTouchEnd = true
        // chart.highlightLineColor = UIColor.systemRed.withAlphaComponent(0.5)
        chart.highlightLineWidth = 5
        chart.delegate = self
        chart.minX = 0
        return chart
    }()
    
    let dataPointLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.backgroundColor = UIColor.clear
        label.textColor = .systemBlue
        label.font = .systemFont(ofSize: 22, weight: .medium)
        label.text = "Drag the line to a data point."
        return label
    }()
    
    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yy h:mm a"
        return formatter
    }()
    
    var dataPoints: [CGFloat] = []
    var dates: [Date] = []
    
    var allSeries: [ChartSeries] = []
    
    func setRoundness(ToCorners corners: UIRectCorner, for view: UIView, withRadius radius: CGFloat) {
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        view.layer.mask = mask
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generalSetup()
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(_:)), name: Notification.Name.myNotificationKey, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        chart.removeAllSeries()
        generateDataPoints()
        chart.setNeedsDisplay()
    }
    
    @objc func notificationReceived(_ notification: Notification) {
        self.tabBarController?.selectedIndex = 0
    }
    
    func generalSetup() {
        navigationItem.title = "Progress"
        view.backgroundColor = .systemBackground
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.label,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .medium)
        ]
        navigationController?.navigationBar.barTintColor = .systemBackground
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.isToolbarHidden = false
        if let toolbar = self.navigationController?.toolbar {
            toolbar.addSubview(dataPointLabel)
            let progressBarInsets = UIEdgeInsets(top: 8, left: 8, bottom: -8, right: -8)
            let progressBarConstraints = [
                dataPointLabel.leadingAnchor.constraint(equalTo: toolbar.leadingAnchor, constant: progressBarInsets.left),
                dataPointLabel.topAnchor.constraint(equalTo: toolbar.topAnchor, constant: progressBarInsets.top),
                dataPointLabel.trailingAnchor.constraint(equalTo: toolbar.trailingAnchor, constant: progressBarInsets.right),
                dataPointLabel.bottomAnchor.constraint(equalTo: toolbar.bottomAnchor, constant: progressBarInsets.bottom)
            ]
            NSLayoutConstraint.activate(progressBarConstraints)
        }
        let rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "save"), style: .plain, target: self, action: #selector(saveImage))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        self.view.addSubview(chart)
        let insets = UIEdgeInsets(top: 6, left: self.view.frame.size.width / 38, bottom: -6, right: -self.view.frame.size.width / 65)
        let lineChartConstraints = [
            chart.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: insets.left),
            chart.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: insets.right),
            chart.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            chart.centerYAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerYAnchor)
        ]
        NSLayoutConstraint.activate(lineChartConstraints)
        
        let portraitAnchorConstant, landscapeAnchorConstant: CGFloat!
        if UIDevice.current.userInterfaceIdiom == .pad {
            portraitAnchorConstant = -screenHeight / 5
            landscapeAnchorConstant = -screenHeight / 10
        } else {
            portraitAnchorConstant = -screenHeight / 3
            landscapeAnchorConstant = -12
        }
        
        portraitAnchor = chart.heightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.heightAnchor, constant: portraitAnchorConstant)
        landscapeAnchor = chart.heightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.heightAnchor, constant: landscapeAnchorConstant)
        
        if (UIWindow.isLandscape) {
            print("Landscape")
            portraitAnchor.isActive = false
            landscapeAnchor.isActive = true
        } else {
            print("Portrait")
            portraitAnchor.isActive = true
            landscapeAnchor.isActive = false
        }
    }
    
    @objc func saveImage() {
        print("save image of progress")
        let imageOfProgress = chart.takeScreenshot()
        UIImageWriteToSavedPhotosAlbum(imageOfProgress, nil, nil, nil)
        showAlert(title: "Successfully Saved!", message: "An image of this graph has been saved to your device's photo library app.") {_ in 
            print("Hello")
        }
    }
    
    private func generateDataPoints() {
        var series: [ChartSeries] = []
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Events")
        let sort = NSSortDescriptor(key: #keyPath(Events.date), ascending: true)
        fetchRequest.sortDescriptors = [sort]
        
        do {
            let fetchedEvents = try managedContext.fetch(fetchRequest) as! [Events]
            print("this is how many (core data) events you are/were subscribed to \(fetchedEvents.count)")
            print("")
            localEvents = fetchedEvents
            
            // let filteredEvents = fetchedEvents.filter({ ($0.completed == true || $0.skipped == true) }).sorted { (first, second) -> Bool in
            //     first.date! < second.date!
            // }
            
            let xValues = localEvents.map({ Double($0.index) })
            let allDates = localEvents.map({ $0.date! })
            chart.xLabels = xValues
            dates = allDates
            
            let singleSeries = ChartSeries([])
            singleSeries.area = true
            
            let filteredEvents = localEvents.filter({ ($0.completed == true) || ($0.skipped == true) })
            for event in filteredEvents {
                if event.skipped == true {
                    let skippedSeries = ChartSeries(data: [(x: Double(event.index) - 0.20, y: 10), (x: Double(event.index) + 0.20, y: 10)])
                    skippedSeries.area = true
                    skippedSeries.line = false
                    skippedSeries.color = .systemOrange
                    
                    let copy = singleSeries.data
                    let intermediate = ChartSeries(data: copy)
                    intermediate.area = true
                    series.append(intermediate)
                    
                    print(singleSeries.data)
                    // series.append(singleSeries)
                    series.append(skippedSeries)
                    singleSeries.data.removeAll()
                } else {
                    singleSeries.data.append((x: Double(event.index), y: Double(event.painScore)))
                }
            }
            if singleSeries.data.count > 0 {
                series.append(singleSeries)
            
                let paddingSeries = ChartSeries(data: [(x: Double(localEvents.count) - 1.0 + 0.28, y: 0)])
                paddingSeries.area = true
                paddingSeries.line = false
                paddingSeries.color = .red
                series.append(paddingSeries)
            }
            chart.xLabelsFormatter = {
                self.dates[(Int(round($1)))].fromCustomFormat(format: "MM/dd/yy") + "\n" + self.dates[(Int(round($1)))].fromCustomFormat(format: "h:mm a")
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        for set in series {
            chart.add(set)
        }
        print(series)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.myNotificationKey, object: nil)
    }
    
}

extension ProgressController {
    
    func didSelectDataPoint(_ index: Int, yValues: Array<CGFloat>) {
        let longFormatter = DateFormatter()
        longFormatter.dateFormat = "MMM d, h:mm a"
        
        let dateString = longFormatter.string(from: dates[index])
        let painScore = yValues.first!
        
        dataPointLabel.text = "Date: \(dateString)     Score: \(painScore)"
        // print("x: \(x) y: \(yValues)")
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        guard let portraitAnchor = portraitAnchor, let landscapeAnchor = landscapeAnchor else { return }
        if UIDevice.current.orientation.isLandscape {
            print("landscape")
            portraitAnchor.isActive = false
            landscapeAnchor.isActive = true
            chart.setNeedsDisplay()
        } else {
            print("portrait")
            portraitAnchor.isActive = true
            landscapeAnchor.isActive = false
            chart.setNeedsDisplay()
        }
    }
    
}

extension ProgressController: ChartDelegate {
    
    func didTouchChart(_ index: Int?, value: Double, exact: Double) {
        let filteredEvents = localEvents.filter({ ($0.skipped == true) }).map({ Int($0.index) })
        if let ind = index {
            if filteredEvents.contains(ind) {
                if (Double(ind) - 0.20) < exact && (Double(ind) + 0.20) > exact {
                    print("print")
                    dataPointLabel.text = "Survey was skipped."
                } else {
                    dataPointLabel.text = "Drag the line to a data point."
                }
            } else {
                if index! < localEvents.count {
                    let score = localEvents[index!].painScore
                    let longFormatter = DateFormatter()
                    longFormatter.dateFormat = "MMM d, h:mm a"
                    let dateString = longFormatter.string(from: dates[ind])
                    dataPointLabel.text = "Date: \(dateString)      Score: \(score)"
                }
            }
        } else {
            for event in filteredEvents {
                if (Double(event) - 0.20) < exact && (Double(event) + 0.20) > exact {
                    print("print")
                    dataPointLabel.text = "Survey was skipped."
                } else {
                    dataPointLabel.text = "Drag the line to a data point."
                }
            }
        }
    }
    
    // func didTouchChart(_ index: Int?, value: Double, exact: Double) {
    //     if index != nil {
    //         let filteredEvents = localEvents.filter({ ($0.skipped == true) }).map({ Int($0.index) })
    //         if filteredEvents.contains(index!) {
    //             if (Double(index!) - 0.20) < exact && (Double(index!) + 0.20) > exact {
    //                 print("print")
    //                 dataPointLabel.text = "Skipped"
    //             } else {
    //                 dataPointLabel.text = "Drag the line to a data point."
    //             }
    //
    //             // print(index)
    //             // dataPointLabel.text = "Skipped"
    //         } else {
    //             if index! < localEvents.count {
    //                 let score = localEvents[index!].painScore
    //                 let longFormatter = DateFormatter()
    //                 longFormatter.dateFormat = "MMM d, h:mm a"
    //                 let dateString = longFormatter.string(from: dates[index!])
    //                 dataPointLabel.text = "Date: \(dateString)      Score: \(score)"
    //             }
    //         }
    //
    //     } else {
    //         let filteredEvents = localEvents.filter({ ($0.skipped == true) }).map({ Int($0.index) })
    //         // if filteredEvents.contains(index!) {
    //
    //         for event in filteredEvents {
    //             if (Double(event) - 0.20) < exact && (Double(event) + 0.20) > exact {
    //                 print("print")
    //                 dataPointLabel.text = "Skipped"
    //             } else {
    //                 dataPointLabel.text = "Drag the line to a data point."
    //             }
    //         }
    //
    //         // print(index)
    //         // dataPointLabel.text = "Skipped"
    //         // }
    //         print(exact)
    //         // dataPointLabel.text = "Drag the line to a data point."
    //     }
    //
    //     // if indexes.count == 0 {
    //     //     dataPointLabel.text = "Drag the line to a data point."
    //     // } else {
    //     //     for (seriesIndex, dataIndex) in indexes.enumerated() {
    //     //         print(seriesIndex)
    //     //         if dataIndex != nil {
    //     //             print(dataIndex)
    //     //             print("series index", seriesIndex)
    //     //             The series at `seriesIndex` is that which has been touched
    //     //             let value = chart.valueForSeries(seriesIndex, atIndex: dataIndex)
    //     //             print(value)
    //     //             let longFormatter = DateFormatter()
    //     //             longFormatter.dateFormat = "MMM d, h:mm a"
    //     //
    //     //             let dateString = longFormatter.string(from: dates[seriesIndex])
    //     //             dataPointLabel.text = "Date: \(dateString)      Score: \(value)"
    //     //         }
    //     //     }
    //     // }
    // }
    
    func didFinishTouchingChart(_ chart: Chart) {
        // print("start")
    }
    
    func didEndTouchingChart(_ chart: Chart) {
        // print("done")
    }
    
}
