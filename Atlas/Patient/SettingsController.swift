//
//  SettingsController.swift
//  Atlas
//
//  Created by Tim Park on 1/9/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import UIKit
import CoreData
import SafariServices

struct Attribution {
    let image: UIImage!
    let name: String!
    let description: String!
    let link: String!
}

class SettingsController: UIViewController, UITableViewDelegate, UITableViewDataSource, SFSafariViewControllerDelegate {
    
    let defaults = UserDefaults.standard
    let notificationDelegate = NotificationDelegate()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let attributions: [Attribution] = [
        Attribution(image: UIImage(named: "firebase"), name: "Firebase", description: "Authentication and Database", link: "https://firebase.google.com"),
        Attribution(image: UIImage(named: "researchKit"), name: "ResearchKit", description: "Survey flow", link: "https://github.com/ResearchKit/ResearchKit"),
        Attribution(image: UIImage(named: "icons8"), name: "Icons8", description: "Tab bar icons", link: "https://icons8.com"),
        Attribution(image: UIImage(named: "flatIcon"), name: "FlatIcon", description: "Vector icons", link: "https://www.flaticon.com"),
    ]
    
    private lazy var tableView: UITableView = {
        let tv = UITableView(frame: .zero, style: .grouped)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.register(GenericCell.self, forCellReuseIdentifier: GenericCell.identifier)
        tv.register(CenteredCell.self, forCellReuseIdentifier: CenteredCell.identifier)
        tv.register(AttributionCell.self, forCellReuseIdentifier: AttributionCell.identifier)
        tv.rowHeight = UITableView.automaticDimension
        tv.estimatedRowHeight = 600.0
        tv.estimatedSectionHeaderHeight = 0
        tv.estimatedSectionFooterHeight = 0
        tv.showsVerticalScrollIndicator = false
        tv.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: Double.leastNormalMagnitude))
        tv.tableFooterView = UIView()
        tv.alwaysBounceVertical = true
        tv.decelerationRate = .fast
        tv.bounces = true
        tv.dataSource = self
        tv.delegate = self
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generalSetup()
        setupTableview()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(_:)), name: Notification.Name.myNotificationKey, object: nil)
    }
    
    func generalSetup() {
        self.navigationItem.title = "Settings"
        self.view.backgroundColor = .systemBackground
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.label,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .medium)
        ]
        self.navigationController?.navigationBar.barTintColor = .systemBackground
        self.navigationController?.navigationBar.isTranslucent = false
        
        let rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "info"), style: .plain, target: self, action: #selector(openPrivacy))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    func setupTableview() {
        self.view.addSubview(tableView)
        let tableViewConstraints = [
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ]
        NSLayoutConstraint.activate(tableViewConstraints)
    }
    
    @objc func removeNotifications() {
        notificationDelegate.notificationCenter.removeAllPendingNotificationRequests()
        print("removed all notifications")
    }
    
    func deleteAllData(_ entity: String) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                managedContext.delete(objectData)
            }
            try managedContext.save()
        } catch let error {
            print("Delete all data in \(entity) error :", error)
        }
    }
    
    @objc func logout() {
        removeNotificationsAndData { (success) in
            (UIApplication.shared.windows.first?.rootViewController as! RootViewController).switchToLogout()
        }
    }
    
    func removeNotificationsAndData(completion: (_ success: Bool) -> Void) {
        guard let patientDashNav = ((self.tabBarController?.viewControllers![0] as? UISplitViewController)?.viewControllers[0]) as? UINavigationController else { return }
        guard let patientDash = patientDashNav.viewControllers.first as? PatientDashboard else { return }
        patientDash.stopTimer(nil)
        
        removeNotifications()
        // don't delete core data
        // deleteAllData("Events")
        
        // didn't really work
        // let domain = Bundle.main.bundleIdentifier!
        // UserDefaults.standard.removePersistentDomain(forName: domain)
        // UserDefaults.standard.synchronize()
        
        // alternative
        defaults.dictionaryRepresentation().keys.forEach(defaults.removeObject(forKey:))
        print("cleared all defaults and data")
        completion(true)
    }
    
    @objc func openPrivacy() {
        let urlString = "https://tmprk.github.io/Atlas-Landing/"
        if let url = URL(string: urlString) {
            let vc = SFSafariViewController(url: url)
            vc.delegate = self
            present(vc, animated: true)
        }
    }
    
    @objc func notificationReceived(_ notification: Notification) {
        self.tabBarController?.selectedIndex = 0
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.myNotificationKey, object: nil)
    }
    
}

extension SettingsController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 1:
            return 1
        case 2:
            return attributions.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0, 1:
                if let cell = tableView.dequeueReusableCell(withIdentifier: GenericCell.identifier) as? GenericCell {
                    if indexPath.row == 0 {
                        cell.keyLabel.text = "Patient I.D."
                        cell.valueLabel.text = defaults.string(forKey: "PATIENT_ID")
                    } else {
                        cell.keyLabel.text = "Procedure"
                        cell.valueLabel.text = defaults.string(forKey: "PROCEDURE")
                    }
                    cell.selectionStyle = .none
                    return cell
                }
            case 2:
                if let cell = tableView.dequeueReusableCell(withIdentifier: CenteredCell.identifier, for: indexPath) as? CenteredCell {
                    cell.actionLabel.text = "Log Out"
//                    cell.actionLabel.text = "Clear Data and Log Out"
                    return cell
                }
            default:
                fatalError()
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: AttributionCell.identifier, for: indexPath) as! AttributionCell
            cell.imageView?.image = UIImage(named: "feedback")
            
            cell.imageView?.layer.masksToBounds = true
            cell.imageView?.layer.cornerRadius = 5.0
            
            cell.textLabel?.text = "Submit User Feedback"
            cell.detailTextLabel?.text = "Any suggestions are welcome!"
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: AttributionCell.identifier, for: indexPath) as! AttributionCell
            cell.imageView?.image = attributions[indexPath.row].image
            
            cell.imageView?.layer.masksToBounds = true
            cell.imageView?.layer.cornerRadius = 5.0
            
            cell.textLabel?.text = attributions[indexPath.row].name
            cell.detailTextLabel?.text = attributions[indexPath.row].description
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if indexPath.row == 2 {
                showAlert(sender: self)
            }
        case 1:
            let urlString = "https://ucsf.kampsite.co"
            if let url = URL(string: urlString) {
                let vc = SFSafariViewController(url: url)
                vc.delegate = self
                present(vc, animated: true)
            }
        case 2:
            let urlString = attributions[indexPath.row].link
            if let url = URL(string: urlString!) {
                let vc = SFSafariViewController(url: url)
                vc.delegate = self
                present(vc, animated: true)
            }
        default:
            break
        }
        if let selectedRows = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectedRows, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionName: String
        switch section {
        case 0:
            sectionName = NSLocalizedString("Data", comment: "Data")
        case 1:
            sectionName = NSLocalizedString("Feedback", comment: "Feedback")
        case 2:
            sectionName = NSLocalizedString("Attributions", comment: "Attributions")
        default:
            sectionName = ""
        }
        return sectionName
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
            cell.preservesSuperviewLayoutMargins = false
        }
    }
    
    func showAlert(sender: AnyObject) {
        let alertController = UIAlertController(title: nil, message: "Are you sure you want to clear data and log out?", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Log out", style: .destructive , handler:{ (UIAlertAction) in
            self.logout()
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction) in
            print("User click Dismiss button")
        }))
        if let popoverController = alertController.popoverPresentationController {
            popoverController.barButtonItem = sender as? UIBarButtonItem
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alertController, animated: true, completion: {
            print("completion block")
        })
    }
    
}
