//
//  TabBarController.swift
//  Atlas
//
//  Created by Tim Park on 11/16/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    var mainViewController: UIViewController!
    
    enum tabBarMenu: Int {
        case Survey
        case Chart
        case Settings
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generalSetup()
    }
    
}

extension TabBarController {
    
    func generalSetup() {
        //        delegate = self
        // self.tabBar.isTranslucent = false
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            let splitViewController = UISplitViewController()
            splitViewController.preferredDisplayMode = .allVisible
            splitViewController.preferredPrimaryColumnWidthFraction = 0.5
            splitViewController.maximumPrimaryColumnWidth = splitViewController.view.bounds.size.width
            
            let patientDashboardController = PatientDashboard()
            let secondViewController = ResponsesController()
            let homeNavigationController = UINavigationController(rootViewController: patientDashboardController)
            let detailedNavigationController = UINavigationController(rootViewController:secondViewController)
            splitViewController.viewControllers = [homeNavigationController, detailedNavigationController]
            mainViewController = splitViewController
            mainViewController.tabBarItem.image = UIImage(named: "survey")
        } else {
            let patientDashboard = PatientDashboard()
            mainViewController = makeNavController(view: patientDashboard, imageName: "survey")
        }
        
        let progressController = ProgressController()
        let settingsController = SettingsController()
        
        let viewControllerList = [
            mainViewController!,
            makeNavController(view: progressController, imageName: "chart"),
            makeNavController(view: settingsController, imageName: "settings")
        ]
        viewControllers = viewControllerList.map { $0 }
        guard let tabBarMenuItem = tabBarMenu(rawValue: 0) else { return }
        setTintColor(forMenuItem: tabBarMenuItem)
    }
}

extension TabBarController {
    
    private func makeNavController(view: UIViewController, imageName: String) -> UINavigationController {
        let viewController = view
        let navController = UINavigationController(navigationBarClass: nil, toolbarClass: Toolbar.self)
        navController.setViewControllers([viewController], animated: false)
        navController.tabBarItem.image = UIImage(named: imageName)
        navController.tabBarItem.title = ""
        return navController
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard
            let menuItemSelected = tabBar.items?.firstIndex(of: item),
            let tabBarMenuItem = tabBarMenu(rawValue: menuItemSelected)
            else { return }
        setTintColor(forMenuItem: tabBarMenuItem)
    }
    
    private func setTintColor(forMenuItem tabBarMenuItem: tabBarMenu) {
        switch tabBarMenuItem {
        case .Survey:
            viewControllers?[tabBarMenuItem.rawValue].tabBarController?.tabBar.tintColor = .systemBlue
        case .Chart:
            viewControllers?[tabBarMenuItem.rawValue].tabBarController?.tabBar.tintColor = .systemBlue
        case .Settings:
            viewControllers?[tabBarMenuItem.rawValue].tabBarController?.tabBar.tintColor = .systemBlue
        }
    }
    
}

//extension TabBarController: UITabBarControllerDelegate {
//
//    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        return TabBarTransition(viewControllers: tabBarController.viewControllers)
//    }
//
//}
