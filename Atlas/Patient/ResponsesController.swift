//
//  ResponsesController.swift
//  Atlas
//
//  Created by Tim Park on 11/11/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

class ResponsesController: UIViewController {
    
    private var tableView: UITableView!
    private var responses: [[String : Any]]!
    
    var dataToPass: SurveyData? {
        didSet {
            if let data = dataToPass {
                self.title = data.title
                self.responses = data.responses
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generalSetup()
        setupTableView()
    }
    
    func generalSetup() {
        self.view.backgroundColor = .systemBackground
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.label,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .medium)
        ]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = .systemBackground
        extendedLayoutIncludesOpaqueBars = true
    }
    
    func setupTableView() {
        tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(ResponseCell.self, forCellReuseIdentifier: ResponseCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.showsVerticalScrollIndicator = true
        tableView.decelerationRate = .fast
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInsetAdjustmentBehavior = .never
        let insetHeaderFrame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 15)
        let view = UIView(frame: insetHeaderFrame)
        self.tableView.tableHeaderView = view
        tableView.tableFooterView = UIView()
        tableView.sectionFooterHeight = 0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600.0
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.alwaysBounceVertical = true
        tableView.bounces = true
        tableView.separatorStyle = .singleLine
        tableView.backgroundColor = .secondarySystemBackground
        
        let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        noDataLabel.text = "Click on a survey you have taken"
        noDataLabel.textColor = UIColor.label
        noDataLabel.textAlignment = .center
        noDataLabel.backgroundColor = .secondarySystemBackground
        tableView.backgroundView = noDataLabel
        tableView.separatorStyle = .none
        
        self.view.addSubview(tableView)
        let tableViewConstraints = [
            tableView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ]
        NSLayoutConstraint.activate(tableViewConstraints)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func reloadTableView() {
        tableView.reloadData()
    }

}

extension ResponsesController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        guard let responses = responses else { return 0 }
        if !responses.isEmpty {
            numOfSections = responses.count
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ResponseCell.identifier, for: indexPath) as! ResponseCell
        let qaPair = responses[indexPath.section]
        switch indexPath.row {
        case 0:
            let question = qaPair["question"] as! String
            cell.configure(fieldText: question)
        case 1:
            let answer = qaPair["answer"] as! String
            cell.configure(fieldText: answer)
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedRows = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectedRows, animated: true)
        }
    }
    
}
