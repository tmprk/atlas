//
//  ChatController.swift
//  Atlas
//
//  Created by Tim Park on 1/16/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import UIKit
import Firebase

class ChatController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var patient: Patient? {
        didSet {
            if let patient = patient {
                self.title = patient.uniqueID + " Inbox"
            }
        }
    }
    
    var isAtBottom: Bool {
        return tableView.contentOffset.y <= -inputContainerView.frame.size.height
    }
    
    var inputAccessoryInsets: UIEdgeInsets {
        return UIEdgeInsets(top: inputContainerView.frame.size.height, left: 0, bottom: 0, right: 0)
    }
    
    let defaults = UserDefaults.standard
    private let db = Firestore.firestore()
    private var tableView: UITableView!
    private var chatMessages = [[ChatMessage]]()
    var lastDocumentSnapshot: DocumentSnapshot!
    var fetchingMore = false
    var patientSenderId: String?
    var keyHeight: CGFloat!
    
    private var messageListener: ListenerRegistration?
    private var reference: CollectionReference?
    
    lazy var inputTextField: ChatField = {
        let textField = ChatField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = .systemBackground
        textField.textColor = .label
        textField.textAlignment = .left
        textField.layer.cornerRadius = 5
        textField.layer.borderWidth = 0.5
        textField.layer.borderColor = UIColor.white.mixDarker(amount: 0.3).cgColor
        textField.clipsToBounds = false
        textField.autocapitalizationType = .none
        textField.placeholder = "Enter message..."
        textField.delegate = self
        // textField.backgroundColor = UIColor.systemPink
        return textField
    }()
    
    lazy var inputContainerView: UIView = {
        let containerView = UIView()
        containerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 55)
        containerView.backgroundColor = UIColor.secondarySystemBackground
        
        let btnImage = UIImage(named: "sendIcon")
        let sendButton = UIButton(type: .system)
        sendButton.contentHorizontalAlignment = .fill
        sendButton.contentVerticalAlignment = .fill
        sendButton.imageView?.contentMode = .scaleAspectFit
        sendButton.setImage(btnImage, for: .normal)
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        sendButton.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        sendButton.imageView!.tintColor = .systemBlue
        // sendButton.backgroundColor = UIColor.systemGreen
        containerView.addSubview(sendButton)
        
        //x,y,w,h
        let insets = UIEdgeInsets(top: 8, left: 8, bottom: -8, right: -8)
        sendButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: insets.top).isActive = true
        sendButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: insets.right).isActive = true
        sendButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: insets.bottom).isActive = true
        sendButton.widthAnchor.constraint(equalTo: containerView.heightAnchor, constant: 2 * insets.bottom).isActive = true
        
        containerView.addSubview(self.inputTextField)
        
        //x,y,x,y
        inputTextField.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: insets.left).isActive = true
        inputTextField.topAnchor.constraint(equalTo: containerView.topAnchor, constant: insets.top).isActive = true
        inputTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor, constant: insets.right).isActive = true
        inputTextField.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: insets.bottom).isActive = true
        
        let separatorLineView = UIView()
        separatorLineView.backgroundColor = UIColor.tertiarySystemBackground
        separatorLineView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(separatorLineView)
        
        //x,y,w,h
        separatorLineView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        separatorLineView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        separatorLineView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        separatorLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        return containerView
    }()
    
    override var inputAccessoryView: UIView? {
        get {
            return inputContainerView
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    var messagesFromServer: [ChatMessage] = [
        // test messages
        
        /*
         ChatMessage(text: "Sixth", sender: "test@gmail.com", date: Date.dateFromCustomString(customString: "08/06/2018 8:10 am")),
         ChatMessage(text: "First", sender: "182193", date: Date.dateFromCustomString(customString: "08/03/2018 8:11 am")),
         ChatMessage(text: "Second", sender: "test@gmail.com", date: Date.dateFromCustomString(customString: "08/04/2018 8:12 am")),
         ChatMessage(text: "Third", sender: "182193", date: Date.dateFromCustomString(customString: "08/05/2018 8:13 am")),
         ChatMessage(text: "Fourth", sender: "test@gmail.com", date: Date.dateFromCustomString(customString: "08/05/2018 8:14 am")),
         ChatMessage(text: "Fifth", sender: "182193", date: Date.dateFromCustomString(customString: "08/06/2018 8:15 am"))
         */
    ]
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        subscribeToShowKeyboardNotifications()
        inputTextField.becomeFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generalSetup()
        paginateData()
        setupTableView()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        handleSend()
        return true
    }
    
    deinit {
        messageListener?.remove()
    }
    
    func generalSetup() {
        self.navigationController?.setToolbarHidden(true, animated: true)
        if let patientId = patient?.uniqueID {
            title = patientId
            print("user is a researcher")
            let diffReference = db.collection("chatrooms").document(patient?.uniqueID ?? patientSenderId!).collection("messages").whereField("date", isGreaterThan: Timestamp(date: Date()))
            messageListener = diffReference.addSnapshotListener { querySnapshot, error in
                guard let snapshot = querySnapshot else {
                    print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
                    return
                }
                snapshot.documentChanges.forEach { change in
                    print("handling document changes")
                    self.handleDocumentChange(change)
                }
            }
        } else {
            title = defaults.string(forKey: "RESEARCHER")
            patientSenderId = defaults.string(forKey: "PATIENT_ID")
            print("user is a patient")
            let diffReference = db.collection("chatrooms").document(patient?.uniqueID ?? patientSenderId!).collection("messages").whereField("date", isGreaterThan: Timestamp(date: Date()))
            messageListener = diffReference.addSnapshotListener { querySnapshot, error in
                guard let snapshot = querySnapshot else {
                    print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
                    return
                }
                snapshot.documentChanges.forEach { change in
                    print("handling document changes")
                    self.handleDocumentChange(change)
                }
            }
        }
        reference = db.collection("chatrooms").document(patient?.uniqueID ?? patientSenderId!).collection("messages")
    }
    
    fileprivate func attemptToAssembleGroupedMessages() {
        chatMessages.removeAll()
        // messagesFromServer = messagesFromServer.sorted(by: { $0.date < $1.date })
        messagesFromServer.sort(by: ({ $0.date.compare($1.date) == .orderedDescending }))
        let groupedMessages = Dictionary(grouping: messagesFromServer) { (element) -> Date in
            return element.date.reduceToMonthDayYear()
        }
        print(groupedMessages as AnyObject)
        let sortedKeys = groupedMessages.keys.sorted()
        sortedKeys.forEach { (key) in
            let values = groupedMessages[key]
            chatMessages.append(values ?? [])
        }
        chatMessages = chatMessages.reversed()
    }
    
    func subscribeToShowKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        let keyboardHeight = keyboardSize.cgRectValue.height
        let keyboardWidth = keyboardSize.cgRectValue.width
        
        keyHeight = keyboardHeight
        
        var contentInsets:UIEdgeInsets
        guard let interfaceOrientation = UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.windowScene?.interfaceOrientation else { return }
        if interfaceOrientation.isPortrait {
            contentInsets = UIEdgeInsets(top: keyboardHeight, left: 0.0, bottom: 0.0, right: 0.0)
        } else {
            contentInsets = UIEdgeInsets(top: keyboardWidth, left: 0.0, bottom: 0.0, right: 0.0)
        }
        tableView.contentInset = contentInsets
        tableView.scrollIndicatorInsets = tableView.contentInset
        
        if chatMessages.count > 0 {
            let lastIndexPath = IndexPath(row: 0, section: 0)
            tableView.scrollToRow(at: lastIndexPath, at: .bottom, animated: true)
        }
        
        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
        
//        print("Content Inset", tableView.contentInset)
//        print("Scroll Indicator Insets", tableView.scrollIndicatorInsets)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        let contentInset: UIEdgeInsets = UIEdgeInsets(top: inputContainerView.frame.size.height, left: 0, bottom: 0, right: 0)
        tableView.contentInset = contentInset
        tableView.scrollIndicatorInsets = tableView.contentInset
        let userInfo = notification.userInfo
        let animationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
        keyHeight = nil
//        print("Content Inset", tableView.contentInset)
//        print("Scroll Indicator Insets", tableView.scrollIndicatorInsets)
    }
    
    func setupTableView() {
        tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(ChatMessageCell.self, forCellReuseIdentifier: ChatMessageCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.showsVerticalScrollIndicator = true
        tableView.decelerationRate = .fast
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: Double.leastNormalMagnitude))
        tableView.sectionHeaderHeight = 0
        tableView.estimatedRowHeight = 75.0
        tableView.estimatedSectionFooterHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.alwaysBounceVertical = true
        tableView.bounces = true
        tableView.separatorStyle = .none
        tableView.keyboardDismissMode = .interactive
        tableView.backgroundColor = .systemBackground
        tableView.contentInset = inputAccessoryInsets
        tableView.scrollIndicatorInsets = inputAccessoryInsets
        
        self.view.addSubview(tableView)
        let tableViewConstraints = [
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ]
        NSLayoutConstraint.activate(tableViewConstraints)
        if chatMessages.count > 0 {
            let lastIndexPath = IndexPath(row: chatMessages.last!.count - 1, section: chatMessages.count - 1)
            tableView.scrollToRow(at: lastIndexPath, at: .bottom, animated: true)
        }
        tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return chatMessages.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let firstMessageInSection = chatMessages[section].first {            
            let dateString = Date.relativeString(date: firstMessageInSection.date)
            let label = DateHeaderLabel()
            label.text = dateString
            
            let containerView = UIView()
            containerView.addSubview(label)
            label.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
            label.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
            
            containerView.transform = CGAffineTransform(scaleX: 1, y: -1)
            return containerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatMessages[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ChatMessageCell.identifier, for: indexPath) as! ChatMessageCell
        let chatMessage = chatMessages[indexPath.section][indexPath.row]
        cell.chatMessage = chatMessage
        cell.transform = CGAffineTransform(scaleX: 1, y: -1)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section, indexPath.row)
    }
    
}

extension ChatController {
    
    @objc func handleSend() {
        var ref: DocumentReference? = nil
        guard let textMessage = inputTextField.text, !textMessage.isEmpty else { return }
        if let patientId = self.patient?.uniqueID {
            // sender is a researcher
            if let email = Auth.auth().currentUser?.email {
                ref = reference!.addDocument(data: [
                    "text": textMessage,
                    "sender": email,
                    "date": Timestamp(date: Date()),
                    "recipient": patientId
                ]) { err in
                    if let err = err {
                        print("Error adding document: \(err)")
                    } else {
                        print("Document added with ID: \(ref!.documentID)")
                    }
                }
                let message = ChatMessage(id: ref!.documentID, text: textMessage, sender: email, date: Date())
                insertNewMessage(message)
            }
        } else {
            // sender is a patient
            let recipient = defaults.string(forKey: "RESEARCHER")
            if let patientId = self.defaults.string(forKey: "PATIENT_ID") {
                ref = reference!.addDocument(data: [
                    "text": textMessage,
                    "sender": patientId,
                    "date": Timestamp(date: Date()),
                    "recipient": recipient!
                ]) { err in
                    if let err = err {
                        print("Error adding document: \(err)")
                    } else {
                        print("Document added with ID: \(ref!.documentID)")
                    }
                }
                let message = ChatMessage(id: ref!.documentID, text: textMessage, sender: patientId, date: Date())
                insertNewMessage(message)
            }
        }
        inputTextField.text = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        print("view disappear")
        guard let splitViewController = splitViewController else { return }
        splitViewController.toggleMasterView()
    }
    
    // Paginates data
    func paginateData() {
        print("Paginating")
        fetchingMore = true
        var query: Query!
        
        if chatMessages.isEmpty {
            query = reference!.order(by: "date", descending: true).limit(to: 10)
            print("First 10 messages loaded")
        } else {
            guard let lastDocumentSnapshot = lastDocumentSnapshot else { return }
            query = reference!.order(by: "date", descending: true).start(afterDocument: lastDocumentSnapshot).limit(to: 5)
            print("Next 5 messages loaded")
        }
        
        query.getDocuments { [weak self] (snapshot, err) in
            if let err = err {
                print("\(err.localizedDescription)")
            } else if snapshot!.isEmpty {
                self?.fetchingMore = false
                return
            } else {
                // let newMessages = snapshot!.documents.compactMap({ try? ChatMessage(dictionary: $0.data()) })
                // self.messagesFromServer = newMessages
                
                guard let documents = snapshot?.documents else { return }
                for document in documents {
                    let dictionary = document.data() as [String : Any]
                    let text = dictionary["text"] as! String
                    let sender = dictionary["sender"] as! String
                    let timestamp = dictionary["date"] as! Timestamp
                    let newMessage = ChatMessage(id: document.documentID, text: text, sender: sender, date: timestamp.dateValue())
                    self?.messagesFromServer.append(newMessage)
                }
                self?.attemptToAssembleGroupedMessages()
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                    self?.tableView.reloadData()
                    self?.fetchingMore = false
                })
                self?.lastDocumentSnapshot = documents.last
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
//        let contentHeight = scrollView.contentSize.height
        
//        print("offset", offsetY)
//        print("content height", contentHeight)
//        print("height minus height", scrollView.frame.height)
//        print("vertical inset bottom", scrollView.verticalOffsetForBottom)
        
//        if scrollView.contentOffset.y <= -inputAccessoryView height, then the tableview reaches the bottom
//        if scrollView.contentOffset.y >= -inputAccessoryView height - keyboard height, then the tableview reaches the top
        
        // add - 50 after scrollView.frame.height to get data earlier
        if (offsetY <= -inputContainerView.frame.size.height) && (scrollView.panGestureRecognizer.translation(in: scrollView.superview).y < 0) {
            // bottom of the screen is reached
            if !fetchingMore {
                print("You're at the bottom")
                print("is at bottom?", isAtBottom)
                print("")
            }
        }
        
        if (offsetY - 50 >= -inputContainerView.frame.size.height - (keyHeight ?? 0)) && (scrollView.panGestureRecognizer.translation(in: scrollView.superview).y > 0) {
            // top of the screen is reached
            if !fetchingMore {
                paginateData()
                print("You're at the top")
                print("is at bottom?", isAtBottom)
                print("")
            }
            print("hello")
        }
        
        print(offsetY)
        print(inputContainerView.frame.size.height - (keyHeight ?? 0))
    }
    
    private func insertNewMessage(_ message: ChatMessage) {
        guard !messagesFromServer.contains(message) else {
            return
        }
        
        messagesFromServer.append(message)
        attemptToAssembleGroupedMessages()
        
        print(chatMessages)
        
        let isLatestMessage = messagesFromServer.firstIndex(of: message) == (messagesFromServer.count - 1)
        let shouldScrollToBottom = isAtBottom && isLatestMessage
        
        tableView.reloadData()
        
        if shouldScrollToBottom {
            print("Scrolling to the bottom")
            DispatchQueue.main.async {
                self.tableView.scrollToBottom(animated: true)
            }
        }
    }
    
    private func handleDocumentChange(_ change: DocumentChange) {
        guard let message = ChatMessage(document: change.document) else {
            return
        }
        switch change.type {
        case .added:
            print("new message inserted")
            insertNewMessage(message)
        default:
            break
        }
    }
    
}
