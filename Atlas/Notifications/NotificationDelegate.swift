//
//  NotificationDelegate.swift
//  Atlas
//
//  Created by Tim Park on 11/11/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit
import Foundation
import UserNotifications

class NotificationDelegate: NSObject, UNUserNotificationCenterDelegate {
    
    typealias CompletionHandler = (_ success:Bool) -> Void
    let notificationCenter = UNUserNotificationCenter.current()
    
    var notifications: [Notif] = [] {
        didSet {
            if !notifications.isEmpty {
                self.schedule(notifications.last!)
            }
        }
    }
    
    func userRequest() {
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if didAllow {
                print("User has allowed notifications")
            } else {
                print("User has denied notifications")
            }
        }
    }
    
    func checkAuthorization(completionHandler: @escaping CompletionHandler) {
        notificationCenter.getNotificationSettings { [weak self] (settings) in
            switch settings.authorizationStatus {
            case .authorized, .provisional:
                print("You have allowed some form of notifications")
                completionHandler(true)
            case .notDetermined:
                self?.userRequest()
            case .denied:
                print("You have not set notifications")
                completionHandler(false)
            @unknown default:
                break
            }
        }
    }
    
    func scheduleNotifications() -> Void {
        // initial notifications
        // [0, 1, 2]
        for i in stride(from: 0, to: notifications.count, by: 1) {
            let noti = notifications[i]
            schedule(noti)
            if !(Procedure(id: UserDefaults.standard.string(forKey: "PROCEDURE")!) == .MBB) {
                if (i + 1) < (notifications.count) {
                    let nextNoti = notifications[i + 1]
                    for index in 1..<2 {
                        let title = noti.id
                        let initialDate = noti.date
                        let date = nextNoti.date.timeIntervalSince(initialDate)
                        let interval = date / 2
                        let timePastInitial = initialDate.addingTimeInterval(Double(index) * interval)
                        let reminderNotif = Notif(title: "Reminder to take survey", body: title + " is overdue. Please take it now.", id: title + "-reminder-\(index)", date: timePastInitial, delivered: false)
                        schedule(reminderNotif)
                    }
                }
            }
        }
    }
    
    func removePendingNotifications(ids: [String]) {
        print("These should be gone:", ids)
        notificationCenter.removePendingNotificationRequests(withIdentifiers: ids)
    }
    
    func schedule(_ notification: Notif) {
        let content = UNMutableNotificationContent()
        let userActions = "User Actions"
        
        content.title = notification.title
        content.body = notification.body
        content.sound = UNNotificationSound.default
        content.badge = 1
        content.categoryIdentifier = userActions
        
        let components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: notification.date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
        
        // let identifier = "Local Notification"
        let request = UNNotificationRequest(identifier: notification.id, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("could not schedule notification")
                print("Error \(error.localizedDescription)")
            }
        }
        
        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [])
        let deleteAction = UNNotificationAction(identifier: "Delete", title: "Delete", options: [.destructive])
        let category = UNNotificationCategory(identifier: userActions,
                                              actions: [snoozeAction, deleteAction],
                                              intentIdentifiers: [],
                                              options: [])
        
        notificationCenter.setNotificationCategories([category])
    }
    
    // func userNotificationCenter(_ center: UNUserNotificationCenter,
    //                             willPresent notification: UNNotification,
    //                             withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    //     print("hello")
    //     completionHandler([.alert,.sound])
    // }
    //
    // func userNotificationCenter(_ center: UNUserNotificationCenter,
    //                             didReceive response: UNNotificationResponse,
    //                             withCompletionHandler completionHandler: @escaping () -> Void) {
    //     if response.notification.request.identifier == "Local Notification" {
    //         print("Handling notifications with the Local Notification Identifier")
    //     }
    //     switch response.actionIdentifier {
    //     case UNNotificationDismissActionIdentifier:
    //         print("Dismiss Action")
    //     case UNNotificationDefaultActionIdentifier:
    //         print("Default")
    //         let identifier = response.notification.request.identifier
    //         if let index = notifications.firstIndex(where: {$0.id == identifier}) {
    //             notifications.remove(at: index)
    //         }
    //         let surveyInfo = ["ORKTask" : SurveyType(id: identifier)?.surveyId()] //optional
    //         NotificationCenter.default.post(name: .myNotificationKey, object: nil, userInfo: surveyInfo)
    //     case "Snooze":
    //         print("Snooze")
    //     case "Delete":
    //         print("Delete")
    //     default:
    //         print("Unknown action")
    //     }
    //     completionHandler()
    // }
}
