//
//  Notifications.swift
//  Atlas
//
//  Created by Tim Park on 11/12/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import Foundation

struct Notif {
    let title: String
    let body: String
    let id: String
    let date: Date
    let delivered: Bool
}

extension Notification.Name {
    public static let myNotificationKey = Notification.Name(rawValue: "myNotificationKey")
}
