//
//  ProgressBar.swift
//  Atlas
//
//  Created by Tim Park on 11/16/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

class ProgressBar: UIView {
    
    var strokeColor: UIColor?
    
    var progress: CGFloat = 0.0 {
        didSet {
            UIView.animate(withDuration: 1) {
                self.setNeedsDisplay()
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        self.backgroundColor = UIColor.clear
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setProgress()
    }
    
    func setProgress() {
        var progress = self.progress
        progress = progress > 1.0 ? progress / 100 : progress
        
        self.layer.cornerRadius = frame.size.height / 2.0
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1.0
        
        let margin: CGFloat = 6.0
        var width = (frame.size.width - margin)  * progress
        let height = frame.size.height - margin
        
        if (width < height) {
            width = height
        }
        
        let pathRef = UIBezierPath(roundedRect: CGRect(x: margin / 2.0, y: margin / 2.0, width: width, height: height), cornerRadius: height / 2.0)
        
        strokeColor?.setFill()
        
        pathRef.fill()
        UIColor.clear.setStroke()
        pathRef.stroke()
        pathRef.close()
    }
}
