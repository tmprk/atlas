//
//  Textfield.swift
//  Atlas
//
//  Created by Tim Park on 10/2/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

class TextField: UITextField {
    
    let iPadPadding = UIEdgeInsets(top: 0, left: 35, bottom: 0, right: 35)
    let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return bounds.inset(by: iPadPadding)
        } else {
            return bounds.inset(by: padding)
        }
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return bounds.inset(by: iPadPadding)
        } else {
            return bounds.inset(by: padding)
        }
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return bounds.inset(by: iPadPadding)
        } else {
            return bounds.inset(by: padding)
        }
    }
    
    override func resignFirstResponder() -> Bool {
        let r = super.resignFirstResponder()
        self.setNeedsLayout()
        self.layoutIfNeeded()
        return r
    }
    
}

class ChatField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func resignFirstResponder() -> Bool {
        let r = super.resignFirstResponder()
        self.setNeedsLayout()
        self.layoutIfNeeded()
        return r
    }
    
}
