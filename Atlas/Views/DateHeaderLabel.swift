//
//  DateHeaderLabel.swift
//  Atlas
//
//  Created by Tim Park on 1/18/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import UIKit

class DateHeaderLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        textColor = .label
        textAlignment = .center
        translatesAutoresizingMaskIntoConstraints = false // enables auto layout
        font = .systemFont(ofSize: 12, weight: .medium)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        let originalContentSize = super.intrinsicContentSize
        let height = originalContentSize.height + 12
        layer.cornerRadius = height / 2
        layer.masksToBounds = true
        return CGSize(width: originalContentSize.width + 20, height: height)
    }
    
}
