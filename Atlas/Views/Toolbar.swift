//
//  Toolbar.swift
//  Atlas
//
//  Created by Tim Park on 11/16/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

class Toolbar: UIToolbar {
    
    let toolbarHeight = UIScreen.main.bounds.height / 15
    
    override func layoutSubviews() {
        super.layoutSubviews()
        frame.size.height = toolbarHeight
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var size = super.sizeThatFits(size)
        size.height = toolbarHeight
        return size
    }
    
}
