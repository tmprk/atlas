//
//  DetailedController.swift
//  Atlas
//
//  Created by Tim Park on 10/2/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit
import Firebase

class DetailedController: UIViewController {
    
    var db = Firestore.firestore()
    private var tableView: UITableView!
    var surveys: [String]!
    
    var patient: Patient? {
        didSet {
            self.title = patient?.uniqueID
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isToolbarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generalSetup()
        setupTableView()
    }
    
    @objc func generalSetup() {
        self.view.backgroundColor = .systemBackground
        self.navigationController?.navigationBar.barTintColor = .systemBackground
        self.navigationController?.isToolbarHidden = false
        let toolbarButtons = [
            UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(showAlert)),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(image: UIImage(named: "chat"), style: .plain, target: self, action: #selector(openChat))
        ]
        toolbarItems = toolbarButtons
//        let rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(showAlert))
//        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    func setupTableView() {
        tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(ResponseCell.self, forCellReuseIdentifier: ResponseCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.showsVerticalScrollIndicator = true
        tableView.decelerationRate = .fast
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInsetAdjustmentBehavior = .never
        let insetHeaderFrame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 15)
        let view = UIView(frame: insetHeaderFrame)
        self.tableView.tableHeaderView = view
        tableView.tableFooterView = UIView()
        tableView.sectionFooterHeight = 0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600.0
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.alwaysBounceVertical = true
        tableView.bounces = true
        tableView.separatorStyle = .singleLine
        self.view.addSubview(tableView)
        let tableViewConstraints = [
            tableView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ]
        NSLayoutConstraint.activate(tableViewConstraints)
    }
    
    @objc func showAlert(sender: AnyObject) {
        let alert = UIAlertController(title: nil, message: "Are you sure you want to delete this entry?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Delete patient data", style: .destructive , handler:{ (UIAlertAction) in
            self.deleteUser()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction) in
            print("User click Dismiss button")
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @objc func openChat() {
        let chatController = ChatController()
        chatController.patient = patient
        self.navigationController?.pushViewController(chatController, animated: true)
    }
    
    func deleteUser() {
        if let patient = patient {
            self.db.collection("patients").document(patient.uniqueID).delete()
            self.navigationController?.popToRootViewController(animated: true)
            print("patient data deleted")
        }
    }
    
}

extension DetailedController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if !surveys!.isEmpty {
            numOfSections = surveys!.count
            tableView.separatorStyle = .singleLine
            tableView.backgroundView = nil
        } else {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "Patient has no surveys"
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ResponseCell.identifier, for: indexPath) as! ResponseCell
        let survey = surveys![indexPath.section]
        cell.fieldLabel.text = survey
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let localSurveyId = surveys[indexPath.section]
        var fetchedResponses: [[String : Any]] = []
        let documentRef = db.collection("patients").document(patient!.uniqueID).collection("surveys").document(localSurveyId)
        documentRef.getDocument(source: .cache) { (documentSnapshot, error) in
            if let fetchedDocument = documentSnapshot {
                if fetchedDocument.exists {
                    let docId = fetchedDocument.documentID
                    print("1: \(docId)")
                    print("2: \(localSurveyId)")
                    if docId == localSurveyId {
                        print("this should print if the document exists in the database")
                        guard let documentData = fetchedDocument.data() else { return }
                        // let surveyCompletionDate = documentData["completionDate"] as! Timestamp
                        let responses = documentData["responses"] as! NSMutableArray
                        for response in responses {
                            let singleResponse = response as! [String : Any]
                            fetchedResponses.append(singleResponse)
                        }
                    }
                    let source = fetchedDocument.metadata.isFromCache ? "local cache" : "server"
                    print("Metadata: Data fetched from \(source)")
                }
                let surveyData = SurveyData(title: localSurveyId, responses: fetchedResponses)
                let responsesController = ResponsesController()
                responsesController.modalPresentationStyle = .formSheet
                responsesController.dataToPass = surveyData
                self.present(UINavigationController(rootViewController: responsesController), animated: true, completion: nil)
                // self.navigationController?.pushViewController(responsesController, animated: true)
            }
            if let selectedRows = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: selectedRows, animated: true)
            }
        }
    }
    
}
