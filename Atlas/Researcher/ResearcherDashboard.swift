//
//  ResearcherDashboard.swift
//  Atlas
//
//  Created by Tim Park on 10/2/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit
import Firebase

class ResearcherDashboard: UIViewController, UINavigationControllerDelegate {
    
    private var db = Firestore.firestore()
    private var tableView: UITableView!
    private var patients = [Patient]()
    var lastDocumentSnapshot: DocumentSnapshot!
    var fetchingMore = false
    let userEmail = Auth.auth().currentUser?.email
    // let cellSpacingHeight: CGFloat = UIScreen.main.bounds.width * 0.04
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupTableView()
        paginateData()
        print("\(userEmail!) authenticated")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let selectedRows = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectedRows, animated: animated)
        }
        self.tableView.reloadData()
    }
    
    func setupListener() {
        db.collection("patients").limit(to: 10).addSnapshotListener { (querySnapshot, error) in
            guard let collection = querySnapshot else {
                print("Error fetching document: \(error!)")
                return
            }
            collection.documentChanges.forEach { [weak self] (diff) in
                if (diff.type == .added) {
                    let newPatientID = diff.document.documentID
                    guard let newPatientType = diff.document.data()["type"] as? String else { return }
                    let procedure = Procedure.init(id: newPatientType)
                    
                    print("reload tableView")
                    self?.patients.insert(Patient(uniqueID: newPatientID, type: procedure!), at: 0)

                    // insert section in table
                    let indexSet = IndexSet(integer: 0)
                    self?.tableView.beginUpdates()
                    self?.tableView.insertSections(indexSet, with: .top)
                    self?.tableView.endUpdates()
                }
            }
            print(self.patients.count)
        }
    }
    
    func setupNavigation() {
        self.title = "Your Patients"
        self.view.backgroundColor = .systemBackground
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.label,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .medium)
        ]
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = .systemBackground
        
        let rightBarButtonItem = UIBarButtonItem(title: "Create", style: .plain, target: self, action: #selector(createNewPatient))
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        let leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(signOut))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    func setupTableView() {
        tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(PatientCell.self, forCellReuseIdentifier: PatientCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = true
        tableView.decelerationRate = .fast
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .clear
        
        tableView.sectionFooterHeight = 0
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: Double.leastNormalMagnitude))
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600.0
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.alwaysBounceVertical = true
        tableView.bounces = true
        
        self.view.addSubview(tableView)
        let tableViewConstraints = [
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ]
        NSLayoutConstraint.activate(tableViewConstraints)
    }
    
    @objc func createNewPatient(sender: AnyObject) {
        let alertController = UIAlertController(title: "New Patient", message: "Select the Type of Procedure for the New Patient", preferredStyle: UIAlertController.Style.actionSheet)
        let procedures = [
            Procedure.ESI,
            Procedure.MBB,
            Procedure.RFA,
            Procedure.JI
        ]
        let closure = { (action: UIAlertAction!) -> Void in
            if let index = alertController.actions.firstIndex(of: action) {
                self.generatePatient(type: procedures[index], sender: sender)
            }
        }
        for procedure in procedures {
            alertController.addAction(UIAlertAction(title: procedure.description(), style: .default, handler: closure))
        }
        let dismiss = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(dismiss)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.barButtonItem = sender as? UIBarButtonItem
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        present(alertController, animated: true, completion: nil)
    }
    
    func generatePatient(type: Procedure, sender: AnyObject) {
        let randomNumber = arc4random_uniform(900000) + 100000
        let alertController = UIAlertController(title: "An entry for the patient has been created. The patient ID is \(randomNumber)", message: nil, preferredStyle: UIAlertController.Style.alert)
        let done = UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) in print("Foo")
        })
        alertController.addAction(done)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.barButtonItem = sender as? UIBarButtonItem
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alertController, animated: true, completion: {
            print("completion block")
        })
        guard let email = Auth.auth().currentUser?.email else { return }
        self.db.collection("patients").document("\(randomNumber)").setData([
            "type": type.description(),
            "researcher": email
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                let patient = Patient(uniqueID: "\(randomNumber)", type: type)
                self.patients.append(patient)
                self.tableView.reloadData()
                print("Document added with ID: \(randomNumber)")
            }
        }
    }
    
    @objc func signOut() {
        try! Auth.auth().signOut()
        UserDefaults.standard.set(false, forKey: "LOGGED_IN")
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print("cleared all defaults")
        (UIApplication.shared.windows.first?.rootViewController as! RootViewController).switchToLogout()
    }
    
}

extension ResearcherDashboard: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numOfSections: Int = 0
        if !patients.isEmpty {
            numOfSections = patients.count
            tableView.backgroundView = nil
        } else {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "No Patients Available"
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var surveys: [String] = []
        
        let patientDocId = patients[indexPath.section]
        let docRef = db.collection("patients").document(patientDocId.uniqueID).collection("surveys")
        
        docRef.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print(document.documentID)
                    surveys.append(document.documentID)
                }
            }
            let detailViewController = DetailedController()
            detailViewController.patient = self.patients[indexPath.section]
            detailViewController.surveys = surveys
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
        if let selectedRows = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectedRows, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return UIScreen.main.bounds.width * 0.02
        }
        return UIScreen.main.bounds.width * 0.04
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.rowHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PatientCell.identifier, for: indexPath) as! PatientCell
        cell.configure(patient: patients[indexPath.section])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.clipsToBounds = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        // print("offsetY: \(offsetY) | contHeight-scrollViewHeight: \(contentHeight-scrollView.frame.height)")
        if offsetY > contentHeight - scrollView.frame.height - 50 {
            // Bottom of the screen is reached
            if !fetchingMore {
                paginateData()
            }
        }
    }
    
    // Paginates data
    func paginateData() {
        guard let email = Auth.auth().currentUser?.email else { return }
        fetchingMore = true
        var query: Query!
        if patients.isEmpty {
            query = db.collection("patients").whereField("researcher", isEqualTo: email).limit(to: 10)
            print("First 10 patients loaded")
        } else {
            query = db.collection("patients").whereField("researcher", isEqualTo: email).start(afterDocument: lastDocumentSnapshot).limit(to: 10)
            print("Next 5 patients loaded")
        }
        query.getDocuments { [weak self] (snapshot, err) in
            if let err = err {
                print("\(err.localizedDescription)")
            } else if snapshot!.isEmpty {
                self?.fetchingMore = false
                return
            } else {
                var emptyPatients: [Patient] = []
                for document in snapshot!.documents {
                    if let type = document.data()["type"] as? String {
                        let patient = Patient(uniqueID: document.documentID, type: Procedure.init(id: type)!)
                        emptyPatients.append(patient)
                    }
                }
                self?.patients.append(contentsOf: emptyPatients)
                DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                    self?.tableView.reloadData()
                    self?.fetchingMore = false
                })
                self?.lastDocumentSnapshot = snapshot!.documents.last
            }
        }
    }
    
}
