//
//  Survey.swift
//  Atlas
//
//  Created by Tim Park on 11/11/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import Foundation
import ResearchKit

class SurveyData {
    
    let title: String
    // let event: Events
    var responses: [[String : Any]]
    
    //    init(title: String, event: Events, responses: [[String : Any]]) {
    //        self.title = title
    //        self.event = event
    //        self.responses = responses
    //    }
    
    init(title: String, responses: [[String : Any]]) {
        self.title = title
        self.responses = responses
    }
    
}

enum SurveyType {
    
    case Minimum_Dataset
    case Follow_Up
    case Minimum_Followup
    
    init?(id: String) {
        switch id {
        case let str where str.contains("Minimum Dataset"):
            self = .Minimum_Dataset
        case let str where str.contains("Follow Up"):
            self = .Follow_Up
        case let str where str.contains("Minimum Followup"):
            self = .Minimum_Followup
        default:
            return nil
        }
    }
    
    func surveyId(title: String? = nil, timeFrame: String? = nil) -> ORKTask! {
        switch self {
        case .Minimum_Dataset:
            return MinimumDataset
        case .Follow_Up:
            var followUpBuilder = FollowUpBuilder()
            followUpBuilder.title = title
            followUpBuilder.timeFrame = timeFrame
            return followUpBuilder.FollowUp
        case .Minimum_Followup:
            return MinimumFollowup
        }
    }
    
}
