//
//  Schedule.swift
//  Atlas
//
//  Created by Tim Park on 11/11/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import ResearchKit

struct Schedule {
    
    var procedure: Procedure
    var startDate: Date
    
    var agenda: [Date : String] {
        switch procedure {
        case .ESI:
            return [
                startDate : "Minimum Dataset",
                startDate.adding(hours: 1) : "Follow Up",
                startDate.adding(hours: 2) : "Follow Up",
                startDate.adding(hours: 3) : "Follow Up",
                startDate.adding(hours: 4) : "Follow Up",
                startDate.adding(hours: 5) : "Follow Up",
                startDate.adding(hours: 6) : "Follow Up",
                startDate.adding(hours: 7) : "Follow Up",
                startDate.adding(hours: 8) : "Follow Up",
                startDate.adding(weeks: 1) : "Minimum Followup",
                startDate.adding(weeks: 2) : "Minimum Followup",
                startDate.adding(months: 1) : "Minimum Followup",
                startDate.adding(months: 3) : "Minimum Followup",
                startDate.adding(months: 6) : "Minimum Followup"
            ]
        case .MBB:
            return [
                startDate : "Minimum Dataset",
                startDate.adding(hours: 1) : "Follow Up",
                startDate.adding(hours: 2) : "Follow Up",
                startDate.adding(hours: 3) : "Follow Up",
                startDate.adding(hours: 4) : "Follow Up",
                startDate.adding(hours: 5) : "Follow Up",
                startDate.adding(hours: 6) : "Follow Up",
                startDate.adding(hours: 7) : "Follow Up",
                startDate.adding(hours: 8) : "Follow Up",
                startDate.adding(days: 1) : "Follow Up",
                startDate.adding(days: 2) : "Follow Up",
                startDate.adding(days: 3) : "Follow Up",
                startDate.adding(days: 4) : "Follow Up",
                startDate.adding(days: 5) : "Follow Up",
                startDate.adding(days: 6) : "Follow Up",
                startDate.adding(days: 7) : "Follow Up"
            ]
        case .RFA:
            return [
                startDate : "Minimum Dataset",
                startDate.adding(weeks: 1) : "Follow Up",
                startDate.adding(weeks: 2) : "Follow Up",
                startDate.adding(weeks: 3) : "Follow Up",
                startDate.adding(months: 1) : "Minimum Followup",
                startDate.adding(months: 3) : "Minimum Followup",
                startDate.adding(months: 6) : "Minimum Followup",
                startDate.adding(months: 12) : "Minimum Followup"
            ]
        case .JI:
            return [
                startDate : "Minimum Dataset",
                startDate.adding(hours: 1) : "Follow Up",
                startDate.adding(hours: 2) : "Follow Up",
                startDate.adding(hours: 3) : "Follow Up",
                startDate.adding(hours: 4) : "Follow Up",
                startDate.adding(hours: 5) : "Follow Up",
                startDate.adding(hours: 6) : "Follow Up",
                startDate.adding(hours: 7) : "Follow Up",
                startDate.adding(hours: 8) : "Follow Up",
                startDate.adding(weeks: 2) : "Follow Up",
                startDate.adding(months: 1) : "Follow Up",
                startDate.adding(months: 3) : "Follow Up",
                startDate.adding(months: 6) : "Follow Up",
            ]
        }
    }
    
}

struct NewSchedule {

    var procedure: Procedure
    var startDate: Date

    var agenda: [[Date : String]:String] {
        switch procedure {
        case .ESI:
            return [
                [startDate : "Minimum Dataset"]: "Initial",
                [startDate.adding(minutes: 1) : "Follow Up"]: "1 Minute",
                [startDate.adding(minutes: 2) : "Follow Up"]: "2 Minute",
                [startDate.adding(minutes: 3) : "Follow Up"]: "3 Minute",
                [startDate.adding(minutes: 4) : "Follow Up"]: "4 Minute",
                [startDate.adding(minutes: 5) : "Follow Up"]: "5 Minute",
                [startDate.adding(minutes: 6) : "Follow Up"]: "6 Minute",
                [startDate.adding(minutes: 7) : "Follow Up"]: "7 Minute",
                [startDate.adding(minutes: 8) : "Follow Up"]: "8 Minute",
                [startDate.adding(minutes: 9) : "Minimum Followup"]: "9 Minute",
                [startDate.adding(minutes: 10) : "Minimum Followup"]: "10 Minute",
                [startDate.adding(minutes: 11) : "Minimum Followup"]: "11 Minute",
                [startDate.adding(minutes: 12) : "Minimum Followup"]: "12 Minute",
                [startDate.adding(minutes: 13) : "Minimum Followup"]: "13 Minute"
            ]
        case .MBB:
            return [
                [startDate : "Minimum Dataset"]: "Initial",
                [startDate.adding(minutes: 3) : "Follow Up"]: "3 Minute",
                [startDate.adding(minutes: 6) : "Follow Up"]: "6 Minute",
                [startDate.adding(minutes: 9) : "Follow Up"]: "9 Minute",
                [startDate.adding(minutes: 12) : "Follow Up"]: "12 Minute",
                [startDate.adding(minutes: 15) : "Follow Up"]: "15 Minute",
                [startDate.adding(minutes: 18) : "Follow Up"]: "18 Minute",
                [startDate.adding(minutes: 21) : "Follow Up"]: "21 Minute",
                [startDate.adding(minutes: 24) : "Follow Up"]: "24 Minute",
                [startDate.adding(minutes: 27) : "Follow Up"]: "27 Minute",
                [startDate.adding(minutes: 30) : "Follow Up"]: "30 Minute",
                [startDate.adding(minutes: 33) : "Follow Up"]: "33 Minute",
                [startDate.adding(minutes: 36) : "Follow Up"]: "36 Minute",
                [startDate.adding(minutes: 39) : "Follow Up"]: "39 Minute",
                [startDate.adding(minutes: 42) : "Follow Up"]: "42 Minute",
                [startDate.adding(minutes: 45) : "Follow Up"]: "45 Minute",
            ]
        case .RFA:
            return [
                [startDate : "Minimum Dataset"]: "Initial",
                [startDate.adding(minutes: 3) : "Follow Up"]: "3 Minute",
                [startDate.adding(minutes: 6) : "Follow Up"]: "6 Minute",
                [startDate.adding(minutes: 9) : "Follow Up"]: "9 Minute",
                [startDate.adding(minutes: 12) : "Minimum Followup"]: "12 Minute",
                [startDate.adding(minutes: 15) : "Minimum Followup"]: "15 Minute",
                [startDate.adding(minutes: 18) : "Minimum Followup"]: "18 Minute",
                [startDate.adding(minutes: 21) : "Minimum Followup"]: "21 Minute"
            ]
        case .JI:
            return [
                [startDate : "Minimum Dataset"]: "Initial",
                [startDate.adding(minutes: 3) : "Follow Up"]: "3 Minute",
                [startDate.adding(minutes: 6) : "Follow Up"]: "6 Minute",
                [startDate.adding(minutes: 9) : "Follow Up"]: "9 Minute",
                [startDate.adding(minutes: 12) : "Follow Up"]: "12 Minute",
                [startDate.adding(minutes: 15) : "Follow Up"]: "15 Minute",
                [startDate.adding(minutes: 18) : "Follow Up"]: "18 Minute",
                [startDate.adding(minutes: 21) : "Follow Up"]: "21 Minute",
                [startDate.adding(minutes: 24) : "Follow Up"]: "24 Minute",
                [startDate.adding(minutes: 27) : "Follow Up"]: "27 Minute",
                [startDate.adding(minutes: 30) : "Follow Up"]: "30 Minute",
                [startDate.adding(minutes: 33) : "Follow Up"]: "33 Minute",
                [startDate.adding(minutes: 36) : "Follow Up"]: "36 Minute",
                [startDate.adding(minutes: 39) : "Follow Up"]: "39 Minute",
                [startDate.adding(minutes: 42) : "Follow Up"]: "42 Minute",
                [startDate.adding(minutes: 45) : "Follow Up"]: "45 Minute",
                [startDate.adding(minutes: 48) : "Follow Up"]: "48 Minute",
                [startDate.adding(minutes: 51) : "Follow Up"]: "51 Minute",
                [startDate.adding(minutes: 54) : "Follow Up"]: "54 Minute",
                [startDate.adding(minutes: 57) : "Follow Up"]: "57 Minute",
                [startDate.adding(minutes: 60) : "Follow Up"]: "60 Minute",
            ]
        }
    }

}

extension NewSchedule {
    // Key and Value are already defined by type dictionary, so it's available here
    func getValue(key: Dictionary<Date, String>) -> String {
        return self.agenda[key]!
    }
}

// the new schedule format with actual time values
// struct NewSchedule {
//
//     var procedure: Procedure
//     var startDate: Date
//
//     var agenda: [[Date : String]:String] {
//         switch procedure {
//         case .ESI:
//             return [
//                 [startDate : "Minimum Dataset"]: "Initial",
//                 [startDate.adding(hours: 1) : "Follow Up"]: "1 Hour",
//                 [startDate.adding(hours: 2) : "Follow Up"]: "2 Hour",
//                 [startDate.adding(hours: 3) : "Follow Up"]: "3 Hour",
//                 [startDate.adding(hours: 4) : "Follow Up"]: "4 Hour",
//                 [startDate.adding(hours: 5) : "Follow Up"]: "5 Hour",
//                 [startDate.adding(hours: 6) : "Follow Up"]: "6 Hour",
//                 [startDate.adding(hours: 7) : "Follow Up"]: "7 Hour",
//                 [startDate.adding(hours: 8) : "Follow Up"]: "8 Hour",
//                 [startDate.adding(weeks: 1) : "Minimum Followup"]: "8 Hour",
//                 [startDate.adding(weeks: 2) : "Minimum Followup"]: "2 Week",
//                 [startDate.adding(months: 1) : "Minimum Followup"]: "1 Month",
//                 [startDate.adding(months: 3) : "Minimum Followup"]: "3 Month",
//                 [startDate.adding(months: 6) : "Minimum Followup"]: "6 Month"
//             ]
//         case .MBB:
//             return [
//                 [startDate : "Minimum Dataset"]: "Initial",
//                 [startDate.adding(hours: 1) : "Follow Up"]: "1 Hour",
//                 [startDate.adding(hours: 2) : "Follow Up"]: "2 Hour",
//                 [startDate.adding(hours: 3) : "Follow Up"]: "3 Hour",
//                 [startDate.adding(hours: 4) : "Follow Up"]: "4 Hour",
//                 [startDate.adding(hours: 5) : "Follow Up"]: "5 Hour",
//                 [startDate.adding(hours: 6) : "Follow Up"]: "6 Hour",
//                 [startDate.adding(hours: 7) : "Follow Up"]: "7 Hour",
//                 [startDate.adding(hours: 8) : "Follow Up"]: "8 Hour",
//                 [startDate.adding(days: 1) : "Follow Up"]: "1 Day",
//                 [startDate.adding(days: 2) : "Follow Up"]: "2 Day",
//                 [startDate.adding(days: 3) : "Follow Up"]: "3 Day",
//                 [startDate.adding(days: 4) : "Follow Up"]: "4 Day",
//                 [startDate.adding(days: 5) : "Follow Up"]: "5 Day",
//                 [startDate.adding(days: 6) : "Follow Up"]: "6 Day",
//                 [startDate.adding(days: 7) : "Follow Up"]: "7 Day",
//             ]
//         case .RFA:
//             return [
//                 [startDate : "Minimum Dataset"]: "Initial",
//                 [startDate.adding(weeks: 1) : "Follow Up"]: "1 Week",
//                 [startDate.adding(weeks: 2) : "Follow Up"]: "2 Week",
//                 [startDate.adding(weeks: 3) : "Follow Up"]: "3 Week",
//                 [startDate.adding(months: 1) : "Minimum Followup"]: "1 Month",
//                 [startDate.adding(months: 3) : "Minimum Followup"]: "3 Month",
//                 [startDate.adding(months: 6) : "Minimum Followup"]: "6 Month",
//                 [startDate.adding(months: 12) : "Minimum Followup"]: "12 Month"
//             ]
//         case .JI:
//             return [
//                 [startDate : "Minimum Dataset"]: "Initial",
//                 [startDate.adding(hours: 1) : "Follow Up"]: "1 Hour",
//                 [startDate.adding(hours: 2) : "Follow Up"]: "2 Hour",
//                 [startDate.adding(hours: 3) : "Follow Up"]: "3 Hour",
//                 [startDate.adding(hours: 4) : "Follow Up"]: "4 Hour",
//                 [startDate.adding(hours: 5) : "Follow Up"]: "5 Hour",
//                 [startDate.adding(hours: 6) : "Follow Up"]: "6 Hour",
//                 [startDate.adding(hours: 7) : "Follow Up"]: "7 Hour",
//                 [startDate.adding(hours: 8) : "Follow Up"]: "8 Hour",
//                 [startDate.adding(hours: 1) : "Follow Up"]: "1 Hour",
//                 [startDate.adding(hours: 2) : "Follow Up"]: "2 Hour",
//                 [startDate.adding(hours: 3) : "Follow Up"]: "3 Hour",
//                 [startDate.adding(hours: 4) : "Follow Up"]: "4 Hour",
//                 [startDate.adding(hours: 5) : "Follow Up"]: "5 Hour",
//                 [startDate.adding(hours: 6) : "Follow Up"]: "6 Hour",
//                 [startDate.adding(hours: 7) : "Follow Up"]: "7 Hour",
//                 [startDate.adding(hours: 8) : "Follow Up"]: "8 Hour",
//                 [startDate.adding(weeks: 2) : "Follow Up"]: "2 Week",
//                 [startDate.adding(months: 1) : "Follow Up"]: "1 Month",
//                 [startDate.adding(months: 3) : "Follow Up"]: "3 Month",
//                 [startDate.adding(months: 6) : "Follow Up"]: "6 Month",
//             ]
//         }
//     }
//
// }

// used this for a test!
// struct TestSchedule {
//
//     var procedure: Procedure
//     var startDate: Date
//
//     var agenda: [[Date : String]:String] {
//         switch procedure {
//         case .ESI:
//             return [
//                 [startDate : "Minimum Dataset"]: "Initial",
//                 [startDate.adding(seconds: 20) : "Follow Up"]: "20 second",
//                 [startDate.adding(seconds: 30) : "Follow Up"]: "30 second",
//                 [startDate.adding(seconds: 40) : "Minimum Followup"]: "40 Second"
//             ]
//         case .MBB:
//             return [
//                 [startDate : "Minimum Dataset"]: "Initial",
//                 [startDate.adding(seconds: 20) : "Follow Up"]: "20 Second",
//                 [startDate.adding(seconds: 40) : "Follow Up"]: "40 Second",
//                 [startDate.adding(seconds: 60) : "Follow Up"]: "60 Minute",
//                 [startDate.adding(seconds: 80) : "Follow Up"]: "80 Minute",
//                 [startDate.adding(seconds: 100) : "Follow Up"]: "100 Minute",
//                 [startDate.adding(seconds: 120) : "Follow Up"]: "120 Minute",
//                 [startDate.adding(seconds: 140) : "Follow Up"]: "140 Minute",
//             ]
//         case .RFA:
//             return [
//                 [startDate : "Minimum Dataset"]: "Initial",
//                 [startDate.adding(seconds: 20) : "Follow Up"]: "20 Second",
//                 [startDate.adding(seconds: 40) : "Follow Up"]: "40 Second",
//             ]
//         case .JI:
//             return [
//                 [startDate : "Minimum Dataset"]: "Initial",
//                 [startDate.adding(seconds: 20) : "Follow Up"]: "20 Second",
//                 [startDate.adding(seconds: 40) : "Follow Up"]: "40 Second",
//             ]
//         }
//     }
//
// }
//
// extension TestSchedule {
//     // Key and Value are already defined by type dictionary, so it's available here
//     func getValue(key: Dictionary<Date, String>) -> String {
//         return self.agenda[key]!
//     }
// }
