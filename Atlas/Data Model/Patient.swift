//
//  Patient.swift
//  Atlas
//
//  Created by Tim Park on 10/2/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

class Patient {
    
    let uniqueID: String
    let type: Procedure
    
    init(uniqueID: String, type: Procedure) {
        self.uniqueID = uniqueID
        self.type = type
    }
    
}

enum Procedure: String, CaseIterable {
    
    case ESI
    case MBB
    case RFA
    case JI
    
    init?(id: String) {
        switch id {
        case "Epidural Steroid Injection (ESI)":
            self = .ESI
        case "Medial Branch Blocks (MBB)":
            self = .MBB
        case "Radiofrequency Ablation (RFA)":
            self = .RFA
        case "Joint Injections (JI)":
            self = .JI
        default:
            return nil
        }
    }
    
    func description() -> String {
        switch self {
        case .ESI:
            return "Epidural Steroid Injection (ESI)"
        case .MBB:
            return "Medial Branch Blocks (MBB)"
        case .RFA:
            return "Radiofrequency Ablation (RFA)"
        case .JI:
            return "Joint Injections (JI)"
        }
    }
    
}
