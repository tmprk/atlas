//
//  SurveyLocal.swift
//  Atlas
//
//  Created by Tim Park on 12/29/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

class SurveyLocal {
    
    var items = [Survey]()
    private var observers = [SurveyUpdateObserver]()
    
    static let shared = SurveyLocal()
    
    init() {
        if let data = UserDefaults.standard.data(forKey: "Survey") {
            if let medication = try? JSONDecoder().decode([Survey].self, from: data) {
                items = medication
            }
        } else {
            reset()
        }
    }
    
    func register(observer: SurveyUpdateObserver) {
        observers.append(observer)
    }
    
    func reset() {
        items.removeAll()
        guard let data = try? JSONEncoder().encode(items) else { return }
        UserDefaults.standard.set(data, forKey: "Survey")
    }
    
    func remove(observer: SurveyUpdateObserver) {
        observers = observers.filter { return $0 !== observer }
    }
    
    func add(medication: Survey) {
        items.append(medication)
        for observer in observers {
            observer.surveysDidUpdate()
        }
        guard let data = try? JSONEncoder().encode(items) else { return }
        UserDefaults.standard.set(data, forKey: "Survey")
    }
    
    func remove(survey: Survey) {
        if let index = items.firstIndex(of: survey) {
            items.remove(at: index)
            for observer in observers {
                observer.surveysDidUpdate()
            }
            guard let data = try? JSONEncoder().encode(items) else { return }
            UserDefaults.standard.set(data, forKey: "Survey")
        }
    }
}

protocol SurveyUpdateObserver: AnyObject {
    func surveysDidUpdate()
}
