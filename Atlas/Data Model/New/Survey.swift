//
//  Survey.swift
//  Atlas
//
//  Created by Tim Park on 12/29/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import UIKit

struct Survey: Equatable, Codable {
    
    static func == (lhs: Survey, rhs: Survey) -> Bool {
        return lhs.title == rhs.title
    }
    
    var title: String
    var completed: Bool
    var date: Date
    var responses: [QuestionAnswer]
    
}

struct QuestionAnswer: Codable {
    
    let question: String
    let answer: String
    
    var dictionary: [String : Any] {
        return [
            "question" : question,
            "answer" : answer
        ]
    }
    
}
