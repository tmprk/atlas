//
//  ChatMessage.swift
//  Atlas
//
//  Created by Tim Park on 1/16/20.
//  Copyright © 2020 Tim Park. All rights reserved.
//

import UIKit
import Firebase

struct ChatMessage {
    
    let id: String?
    let text: String
    let sender: String
    let date: Date
    
    //    init(dictionary: [String: Any]) {
    //        self.text = dictionary["text"] as? String ?? ""
    //        self.sender = dictionary["isIncoming"] as? String ?? ""
    //        self.date = dictionary["date"] as? Date ?? Date()
    //    }
    //    var description: String {
    //        return text  + "from " + sender
    //    }
    
}

extension ChatMessage: Codable {
    
    init?(document: QueryDocumentSnapshot) {
        let data = document.data()
        
        guard let text = data["text"] as? String else {
            return nil
        }
        guard let sender = data["sender"] as? String else {
            return nil
        }
        guard let timestamp = data["date"] as? Timestamp else {
            return nil
        }
        
        self.text = text
        self.date = timestamp.dateValue()
        self.sender = sender
        id = document.documentID
        
    }
    
}


extension ChatMessage: Comparable {
    
    static func == (lhs: ChatMessage, rhs: ChatMessage) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func <(lhs: ChatMessage, rhs: ChatMessage) -> Bool {
        return lhs.date < rhs.date
    }
    
}
