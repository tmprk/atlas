//
//  Response.swift
//  Atlas
//
//  Created by Tim Park on 11/11/19.
//  Copyright © 2019 Tim Park. All rights reserved.
//

import Foundation

struct Response {
    
    let question: String
    let answer: String
    
    var dictionary: [String : Any] {
        return [
            "question" : question,
            "answer" : answer
        ]
    }
    
    //    init?(dictionary: [[String: Any]]) {
    //        self.question = dictionary["question"] as! String
    //        self.answer = dictionary["answer"] as! String
    //    }
    
//    init?(dictionary: [String: Any]) {
//        self.question = dictionary["question"] as! String
//        self.answer = dictionary["answer"] as! String
//    }
    
}
