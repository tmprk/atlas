const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

exports.notifyNewMessage = functions.firestore
  .document('chatrooms/{patient}/messages/{message}')
  .onCreate((docSnapshot, context) => {
    const message = docSnapshot.data();
    const recipientId = message['recipient'];
    const senderName = message['sender'];

    if (recipientId.indexOf('@') > 0) {
      return admin
        .firestore()
        .doc('researchers/' + recipientId)
        .get()
        .then((userDoc) => {
          const registrationTokens = userDoc.get('fcmToken');

          const notificationBody =
            message['type'] === 'TEXT' ? message['text'] : 'You received a new message.';
          const payload = {
            notification: {
              title: senderName + ' sent you a message.',
              body: notificationBody,
              clickAction: 'ChatActivity',
            },
            data: {
              USER_NAME: senderName,
              USER_ID: message['sender'],
            },
          };

          return admin
            .messaging()
            .sendToDevice(registrationTokens, payload)
            .then((response) => {
              const stillRegisteredTokens = registrationTokens;

              response.results.forEach((result, index) => {
                const error = result.error;
                if (error) {
                  const failedRegistrationToken = registrationTokens[index];
                  console.error('blah', failedRegistrationToken, error);
                  if (
                    error.code === 'messaging/invalid-registration-token' ||
                    error.code === 'messaging/registration-token-not-registered'
                  ) {
                    const failedIndex = stillRegisteredTokens.indexOf(failedRegistrationToken);
                    if (failedIndex > -1) {
                      stillRegisteredTokens.splice(failedIndex, 1);
                    }
                  }
                }
              });
              return admin
                .firestore()
                .doc('researchers/' + recipientId)
                .update({
                  registrationTokens: stillRegisteredTokens,
                });
            });
        });
    } else {
      return admin
        .firestore()
        .doc('patients/' + recipientId)
        .get()
        .then((userDoc) => {
          const registrationTokens = userDoc.get('fcmToken');

          const notificationBody =
            message['type'] === 'TEXT' ? message['text'] : 'You received a new message.';
          const payload = {
            notification: {
              title: senderName + ' sent you a message.',
              body: notificationBody,
              clickAction: 'ChatActivity',
            },
            data: {
              USER_NAME: senderName,
              USER_ID: message['sender'],
            },
          };

          return admin
            .messaging()
            .sendToDevice(registrationTokens, payload)
            .then((response) => {
              const stillRegisteredTokens = registrationTokens;

              response.results.forEach((result, index) => {
                const error = result.error;
                if (error) {
                  const failedRegistrationToken = registrationTokens[index];
                  console.error('blah', failedRegistrationToken, error);
                  if (
                    error.code === 'messaging/invalid-registration-token' ||
                    error.code === 'messaging/registration-token-not-registered'
                  ) {
                    const failedIndex = stillRegisteredTokens.indexOf(failedRegistrationToken);
                    if (failedIndex > -1) {
                      stillRegisteredTokens.splice(failedIndex, 1);
                    }
                  }
                }
              });
              return admin
                .firestore()
                .doc('patients/' + recipientId)
                .update({
                  registrationTokens: stillRegisteredTokens,
                });
            });
        });
    }
  });
